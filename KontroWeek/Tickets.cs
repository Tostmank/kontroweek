﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Diagnostics;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Reflection;

namespace KontroWeek
{
    public partial class Tickets : Form
    {
        // Инфраструктура для доступа к БД
        private OleDbConnection _connection;
        private int month, year;

        public Tickets()
        {
            InitializeComponent();
            _connection = Statics.odbCon;
            var strSql = @"SELECT * FROM Отчёты";
            var reportDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var report = new DataTable();
            reportDt.Fill(report);
            comB_report.DataSource = report;
            comB_report.DisplayMember = "Отчёт";

            month = DateTime.Today.Month;
            year = DateTime.Today.Year;

            //month = 12;
            //month = 10;
            //year = 2014;
        }

        private void Tickets_FormClosing(object sender, FormClosingEventArgs e)
        {
            Statics.MainFrm.Show();
            Statics.MainFrm.Activate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(@"Вы уверены, что хотите подготовить отчёты ?", "", MessageBoxButtons.YesNo) ==
               DialogResult.No) return;
            var swatch = new Stopwatch();
            swatch.Start();
            switch (comB_report.Text)
            {
                case "Все отчёты":
                    Лучшие();
                    Неусп();
                    Итоги();
                    //Приказ
                    //Отчёт
                    break;
                case "Лучшие":
                    Лучшие();
                    break;
                case "Неуспевающие":
                    Неусп();
                    break;
                case "Итоги":
                    Итоги();
                    break;
                case "Отчёт":
                    //Отчёт
                    break;
            }
            /* Kopia   'Резервное копирование данных за контрольную неделю, кроме папки приказов,
                'она копируется на следующей конрольной неделе, так как после подготовки отчётов в них вносятся дополнения и изменения*/

            swatch.Stop(); // стоп   
            this.Text = swatch.Elapsed.ToString(); // результат
            MessageBox.Show(@"Подготовка отчётов завершена!", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Лучшие()
        {
            var t1 = year - 2000;
            var t2 = 0;
            if (month > 0 && month < 4) t2 = -1;
            else if (month > 3 && month < 9) t2 = 0;
            else if (month > 8 && month < 11) t2 = 1;
            else if (month > 10 && month < 13) t2 = 2;

            int kn;
            if ((month > 0 && month < 4) || month == 9 || month == 10) kn = 1;
            else kn = 2;

            // Студенты
            var strSql =
                @"SELECT (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9) AS Курс, Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"FROM Уч_Отделы INNER JOIN (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) ON Уч_Отделы.КодУч = Группы.Уч_Отдел " +
                @"GROUP BY Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"ORDER BY (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9), Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО";
            var studDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var stud = new DataTable();
            studDt.Fill(stud);

            // Средний балл
            strSql = String.Format(
                "SELECT Балл_КН.КодСт, Балл_КН.Балл " +
                "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт " +
                "WHERE (((Балл_КН.[№КН])=4*({0}-Right(([Группы].[Группа]),2))+{1})) " +
                "ORDER BY Балл_КН.КодСт", t1, t2);
            var avgDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var avg = new DataTable();
            avgDt.Fill(avg);

            var avgball = (from avgRow in avg.AsEnumerable()
                           group avgRow by avgRow.ItemArray[0]
                               into grouping
                               orderby grouping.Key
                               select new
                               {
                                   КодСт = grouping.Key,
                                   avgb = grouping.Average(xx => Convert.ToInt32(xx.ItemArray[1]))
                               }).CopyToDataTable();


            //Проп_Сейч
            strSql = String.Format("SELECT Студенты.КодСт, Успеваемость.Пропуск_уваж, Успеваемость.Пропуск_неуваж" +
                                   " FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент" +
                                   " WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1,
                t2);
            var propNowDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var propNow = new DataTable();
            propNowDt.Fill(propNow);


            //Проп_Пред
            strSql =
                String.Format(
                    "SELECT Студенты.КодСт, [Пропуск_уваж]*{2} AS Проп_Пред_Ув, [Пропуск_неуваж]*{2} AS Проп_Пред_Неув " +
                    "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент " +
                    "WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1, t2 - kn + 1,
                    kn - 1);
            var propDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var prop = new DataTable();
            propDt.Fill(prop);

            //Параметры для взысканий
            var rsPVzDt = new OleDbDataAdapter(new OleDbCommand("SELECT * FROM Парам_Взыск", _connection));
            var rsPVz = new DataTable();
            rsPVzDt.Fill(rsPVz);
            var rs0 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[0]);
            var rs1 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[1]);
            var rs2 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[2]);
            var rs3 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[3]);
            var rs4 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[4]);
            var rs5 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[5]);
            var rs6 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[6]);
            var rs7 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[7]);
            var rs8 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[8]);
            var rs9 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[9]);
            var rs10 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[10]);

            DataTable rs = null;
            if (kn == 1)
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") > rs6) &&
                             (propRow.Field<int>("Пропуск_неуваж") < rs3))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propRow.Field<int>("Пропуск_неуваж")
                      }).CopyToDataTable();
            }
            else
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propNowRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propNowRow.Field<int>("КодСт")
                      join propRow in prop.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") > rs6) &&
                             (propNowRow.Field<int>("Пропуск_неуваж") < rs3))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propNowRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propNowRow.Field<int>("Пропуск_неуваж"),
                          Проп_Пред_Ув = propRow.Field<int>("Проп_Пред_Ув"),
                          Проп_Пред_Неув = propRow.Field<int>("Проп_Пред_Неув")
                      }).CopyToDataTable();
            }

            if (rs.Rows.Count == 0)
            {
                MessageBox.Show(@"Лучших студентов нет!");
                return;
            }
            pBar.Maximum = rs.Rows.Count;
            pBar.Value = 0;

            //Работа с файлами
            //Проверка наличия директории - если её нет, то создаём
            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Приказы\"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Приказы\");


            var wdApp = new Word.Application { Visible = false, ScreenUpdating = false };
            Word.Document doc;
            Word.Range rngCur;
            try
            {
                var count = 0;
                while (count <= rs.Rows.Count)
                {
                    wdApp.ScreenUpdating = false;

                    doc = wdApp.Documents.Open(Directory.GetCurrentDirectory() +
                                               @"\Оформление приказа\" + @"Бланк_Лучшие.doc");
                    rngCur = doc.Content;

                    var kurs = Convert.ToInt32(rs.Rows[count].ItemArray[0]);

                    //№ КН
                    rngCur.Find.Text = "{А_№КН}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn.ToString();
                    //Семестр
                    rngCur.Find.Text = "{А_Сем}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();

                    if (month > 0 && month < 9)
                        rngCur.Text = "весеннего";
                    else
                        rngCur.Text = "осеннего";

                    //Учебные года
                    rngCur.Find.Text = "{А_Года}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    if (month > 0 && month < 9)
                        rngCur.Text = (year - 1) + @"/" + year;
                    else
                        rngCur.Text = Text = (year) + @"/" + (year + 1);
                    //Курс
                    rngCur.Find.Text = "{А_Курс}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kurs.ToString();
                    //Средний балл
                    rngCur.Find.Text = "{А_СрБ}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = Math.Round(rs6, 2).ToString();
                    //Пропусков для взыскания
                    rngCur.Find.Text = "{А_Пр}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn == 1 ? rs3.ToString() : rs7.ToString();
                    //Заголовок в таблице
                    rngCur.Find.Text = "{А_ЗагТ}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn == 1 ? "Пропуски с начала семестра" : "Пропуски с начала семестра /после взыскания";

                    var myTable = doc.Tables[1];
                    var x = 4;
                    if (kn == 1)
                    {
                        while (Convert.ToInt32(rs.Rows[count].ItemArray[0]) == kurs)
                        {
                            var row = rs.Rows[count].ItemArray;
                            myTable.Range.Rows.Add();
                            myTable.Cell(x, 1).Range.Text = (x - 3).ToString();
                            myTable.Cell(x, 2).Range.Text = String.Format("{0} {1}.{2}", row[3],
                                row[4].ToString()[0], row[4].ToString().Length == 1 ? ' ' : row[4].ToString()[1]);
                            myTable.Cell(x, 3).Range.Text = row[2].ToString();
                            myTable.Cell(x, 4).Range.Text = row[6].ToString();
                            myTable.Cell(x, 5).Range.Text = row[7].ToString();
                            myTable.Cell(x, 6).Range.Text = Math.Round(Convert.ToDouble(row[5]), 2).ToString();
                            x++;
                            count++;
                            pBar.Value = count;
                            if (count > rs.Rows.Count - 1) break;
                        }
                    }
                    else
                    {
                        while (Convert.ToInt32(rs.Rows[count].ItemArray[0]) == kurs)
                        {
                            var row = rs.Rows[count].ItemArray;
                            myTable.Range.Rows.Add();
                            myTable.Cell(x, 1).Range.Text = (x - 3).ToString();
                            myTable.Cell(x, 2).Range.Text = String.Format("{0} {1}.{2}", row[3],
                                row[4].ToString()[0], row[4].ToString().Length == 1 ? ' ' : row[4].ToString()[1]);
                            myTable.Cell(x, 3).Range.Text = row[2].ToString();
                            myTable.Cell(x, 4).Range.Text = row[6] + @"/" +
                                                            (Convert.ToInt32(row[6]) - Convert.ToInt32(row[8]));
                            myTable.Cell(x, 5).Range.Text = row[7] + @"/" +
                                                            (Convert.ToInt32(row[7]) - Convert.ToInt32(row[9]));
                            myTable.Cell(x, 6).Range.Text = Math.Round(Convert.ToDouble(row[5]), 2).ToString();
                            x++;
                            count++;
                            pBar.Value = count;
                            if (count > rs.Rows.Count - 1) break;
                        }
                    }

                    wdApp.ScreenUpdating = true;
                    // Запрашивать сохранение
                    wdApp.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;
                    //Создаём файл

                    doc.SaveAs(Directory.GetCurrentDirectory() + @"\Приказы\" + @"Лучшие " + kurs + @" курс",
                        Word.WdSaveFormat.wdFormatDocument97);
                    doc.Close();

                    count++;
                }
            }
            catch (Exception z)
            {
                MessageBox.Show("Покрашились " + z.Message);
            }
            finally
            {
                wdApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(wdApp);
                doc = null;
                wdApp = null;
                GC.Collect();
            }

        }

        private void Неусп()
        {
            //Работа с файлами
            //Проверка наличия директории - если её нет, то создаём
            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Приказы\"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Приказы\");

            var t1 = year - 2000;
            var t2 = 0;
            if (month > 0 && month < 4) t2 = -1;
            else if (month > 3 && month < 9) t2 = 0;
            else if (month > 8 && month < 11) t2 = 1;
            else if (month > 10 && month < 13) t2 = 2;

            int kn;
            if ((month > 0 && month < 4) || month == 9 || month == 10) kn = 1;
            else kn = 2;

            // Студенты
            var strSql =
                @"SELECT (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9) AS Курс, Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"FROM Уч_Отделы INNER JOIN (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) ON Уч_Отделы.КодУч = Группы.Уч_Отдел " +
                @"GROUP BY Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"ORDER BY (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9), Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО";
            var studDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var stud = new DataTable();
            studDt.Fill(stud);

            // Средний балл
            strSql = String.Format(
                "SELECT Балл_КН.КодСт, Балл_КН.Балл " +
                "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт " +
                "WHERE (((Балл_КН.[№КН])=4*({0}-Right(([Группы].[Группа]),2))+{1})) " +
                "ORDER BY Балл_КН.КодСт", t1, t2);
            var avgDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var avg = new DataTable();
            avgDt.Fill(avg);

            var avgball = (from avgRow in avg.AsEnumerable()
                           group avgRow by avgRow.ItemArray[0]
                               into grouping
                               orderby grouping.Key
                               select new
                               {
                                   КодСт = grouping.Key,
                                   avgb = grouping.Average(xx => Convert.ToInt32(xx.ItemArray[1]))
                               }).CopyToDataTable();


            //Проп_Сейч
            strSql = String.Format("SELECT Студенты.КодСт, Успеваемость.Пропуск_уваж, Успеваемость.Пропуск_неуваж" +
                                   " FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент" +
                                   " WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1,
                t2);
            var propNowDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var propNow = new DataTable();
            propNowDt.Fill(propNow);


            //Проп_Пред
            strSql =
                String.Format(
                    "SELECT Студенты.КодСт, [Пропуск_уваж]*{2} AS Проп_Пред_Ув, [Пропуск_неуваж]*{2} AS Проп_Пред_Неув " +
                    "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент " +
                    "WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1, t2 - kn + 1,
                    kn - 1);
            var propDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var prop = new DataTable();
            propDt.Fill(prop);

            //Параметры для взысканий
            var rsPVzDt = new OleDbDataAdapter(new OleDbCommand("SELECT * FROM Парам_Взыск", _connection));
            var rsPVz = new DataTable();
            rsPVzDt.Fill(rsPVz);
            var rs0 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[0]);
            var rs1 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[1]);
            var rs2 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[2]);
            var rs3 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[3]);
            var rs4 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[4]);
            var rs5 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[5]);
            var rs6 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[6]);
            var rs7 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[7]);
            var rs8 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[8]);
            var rs9 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[9]);
            var rs10 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[10]);

            DataTable rs = null;
            if (kn == 1)
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") <= rs0) ||
                             (propRow.Field<int>("Пропуск_неуваж") >= rs3))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propRow.Field<int>("Пропуск_неуваж")
                      }).CopyToDataTable();
            }
            else
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propNowRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propNowRow.Field<int>("КодСт")
                      join propRow in prop.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") <= rs0) ||
                             (propNowRow.Field<int>("Пропуск_неуваж") >= rs7))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propNowRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propNowRow.Field<int>("Пропуск_неуваж"),
                          Проп_Пред_Ув = propRow.Field<int>("Проп_Пред_Ув"),
                          Проп_Пред_Неув = propRow.Field<int>("Проп_Пред_Неув")
                      }).CopyToDataTable();
            }

            if (rs.Rows.Count == 0)
            {
                MessageBox.Show(@"Неуспевающих студентов нет!");
                return;
            }
            pBar.Maximum = rs.Rows.Count;
            pBar.Value = 0;

            var wdApp = new Word.Application { Visible = false, ScreenUpdating = false };
            Word.Document doc;
            Word.Range rngCur;
            try
            {
                var count = 0;
                while (count <= rs.Rows.Count)
                {
                    wdApp.ScreenUpdating = false;

                    doc = wdApp.Documents.Open(Directory.GetCurrentDirectory() +
                                               @"\Оформление приказа\" + @"Бланк_Неуспевающие.doc");
                    rngCur = doc.Content;

                    var kurs = Convert.ToInt32(rs.Rows[count].ItemArray[0]);

                    //№ КН
                    rngCur.Find.Text = "{А_№КН}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn.ToString();
                    //Семестр
                    rngCur.Find.Text = "{А_Сем}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();

                    if (month > 0 && month < 9)
                        rngCur.Text = "весеннего";
                    else
                        rngCur.Text = "осеннего";

                    //Учебные года
                    rngCur.Find.Text = "{А_Года}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    if (month > 0 && month < 9)
                        rngCur.Text = (year - 1) + @"/" + year;
                    else
                        rngCur.Text = Text = (year) + @"/" + (year + 1);
                    //Курс
                    rngCur.Find.Text = "{А_Курс}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kurs.ToString();
                    //Средний балл
                    rngCur.Find.Text = "{А_СрБ}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = Math.Round(rs0, 2).ToString();
                    //Пропусков для взыскания
                    rngCur.Find.Text = "{А_Пр}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn == 1 ? rs3.ToString() : rs7.ToString();
                    //Заголовок в таблице
                    rngCur.Find.Text = "{А_ЗагТ}";
                    rngCur.Find.Forward = true;
                    rngCur.Find.Wrap = Word.WdFindWrap.wdFindContinue;
                    rngCur.Find.Execute();
                    rngCur.Text = kn == 1 ? "Пропуски с начала семестра" : "Пропуски с начала семестра /после взыскания";

                    var myTable = doc.Tables[1];
                    var x = 4;
                    if (kn == 1)
                    {
                        while (Convert.ToInt32(rs.Rows[count].ItemArray[0]) == kurs)
                        {
                            var row = rs.Rows[count].ItemArray;
                            myTable.Range.Rows.Add();
                            myTable.Cell(x, 1).Range.Text = (x - 3).ToString();
                            myTable.Cell(x, 2).Range.Text = String.Format("{0} {1}.{2}", row[3],
                                row[4].ToString()[0], row[4].ToString().Length == 1 ? ' ' : row[4].ToString()[1]);
                            myTable.Cell(x, 3).Range.Text = row[2].ToString();
                            myTable.Cell(x, 4).Range.Text = row[6].ToString();
                            myTable.Cell(x, 5).Range.Text = row[7].ToString();
                            myTable.Cell(x, 6).Range.Text = Math.Round(Convert.ToDouble(row[5]), 2).ToString();

                            if ((Convert.ToInt32(row[7]) >= rs5) || (Convert.ToDouble(row[5]) <= rs2))
                            {
                                if (Convert.ToInt32(row[7]) >= rs5)
                                    myTable.Cell(x, 7)
                                        .Range.InsertAfter(Convert.ToDouble(row[5]) <= rs2
                                            ? "Выг Проп и Усп"
                                            : "Выг Проп");
                                else
                                    myTable.Cell(x, 7).Range.InsertAfter("Выг Усп");
                            }
                            else if ((Convert.ToInt32(row[7]) >= rs4) || (Convert.ToDouble(row[5]) <= rs1))
                                if (Convert.ToInt32(row[7]) >= rs4)
                                    myTable.Cell(x, 7)
                                        .Range.InsertAfter(Convert.ToDouble(row[5]) <= rs1
                                            ? "Зам Проп и Усп"
                                            : "Зам Проп");
                                else
                                    myTable.Cell(x, 7).Range.InsertAfter("Зам Усп");
                            else if (Convert.ToInt32(row[7]) >= rs3)
                                myTable.Cell(x, 7)
                                    .Range.InsertAfter(Convert.ToDouble(row[5]) <= rs0
                                        ? "Пред Проп и Усп"
                                        : "Пред Проп");
                            else
                                myTable.Cell(x, 7).Range.InsertAfter("Пред Усп");

                            x++;
                            count++;
                            pBar.Value = count;
                            if (count > rs.Rows.Count - 1) break;
                        }
                    }
                    else
                    {
                        while (Convert.ToInt32(rs.Rows[count].ItemArray[0]) == kurs)
                        {
                            var row = rs.Rows[count].ItemArray;
                            var vzisk = "";

                            if ((Convert.ToInt32(row[7]) >= rs9) || (Convert.ToDouble(row[5]) <= rs2))
                            {
                                if (Convert.ToInt32(row[7]) >= rs9)
                                {
                                    if (Convert.ToInt32(row[9]) >= rs5)
                                    {
                                        if ((Convert.ToInt32(row[7]) - Convert.ToInt32(row[9])) >= rs10)
                                            vzisk = Convert.ToDouble(row[5]) <= rs2 ? "Выг Проп и Усп" : "Выг Проп";
                                        else if (Convert.ToDouble(row[5]) <= rs2)
                                            vzisk = "Выг Усп";
                                        else if (Convert.ToDouble(row[5]) <= rs1)
                                            vzisk = "Зам Усп";
                                        else
                                            vzisk = Convert.ToDouble(row[5]) <= rs0 ? "Пред Усп" : "";
                                    }
                                    else
                                        vzisk = Convert.ToDouble(row[5]) <= rs2 ? "Выг Проп и Усп" : "Выг Проп";
                                }
                                else
                                    vzisk = "Выг Усп";
                            }
                            else
                            {
                                if ((Convert.ToInt32(row[7]) >= rs8) || (Convert.ToDouble(row[5]) <= rs1))
                                {
                                    if (Convert.ToInt32(row[7]) >= rs8)
                                    {
                                        if (Convert.ToInt32(row[9]) < rs4)
                                            vzisk = Convert.ToDouble(row[5]) <= rs1 ? "Зам Проп и Усп" : "Зам Проп";
                                        else if (Convert.ToDouble(row[5]) <= rs1)
                                            vzisk = "Зам Усп";
                                        else
                                            vzisk = Convert.ToDouble(row[5]) <= rs0 ? "Пред Усп" : "";
                                    }
                                    else
                                        vzisk = "Зам Усп";
                                }
                                else
                                {
                                    if (Convert.ToInt32(row[7]) >= rs7)
                                    {
                                        if (Convert.ToInt32(row[9]) < rs3)
                                            vzisk = Convert.ToDouble(row[5]) <= rs0 ? "Пред Проп и Усп" : "Пред Проп";
                                        else
                                            vzisk = Convert.ToDouble(row[5]) <= rs0 ? "Пред Усп" : "";
                                    }
                                    else
                                        vzisk = "Пред Усп";
                                }
                            }

                            if (vzisk != "")
                            {
                                myTable.Range.Rows.Add();
                                myTable.Cell(x, 1).Range.Text = (x - 3).ToString();
                                myTable.Cell(x, 2).Range.Text = String.Format("{0} {1}.{2}", row[3],
                                    row[4].ToString()[0], row[4].ToString().Length == 1 ? ' ' : row[4].ToString()[1]);
                                myTable.Cell(x, 3).Range.Text = row[2].ToString();
                                myTable.Cell(x, 4).Range.Text = row[6] + @"/" +
                                                                (Convert.ToInt32(row[6]) - Convert.ToInt32(row[8]));
                                myTable.Cell(x, 5).Range.Text = row[7] + @"/" +
                                                                (Convert.ToInt32(row[7]) - Convert.ToInt32(row[9]));
                                myTable.Cell(x, 6).Range.Text = Math.Round(Convert.ToDouble(row[5]), 2).ToString();
                                myTable.Cell(x, 7).Range.InsertAfter(vzisk);
                                x++;
                            }
                            count++;
                            pBar.Value = count;
                            if (count > rs.Rows.Count - 1) break;
                        }
                    }

                    wdApp.ScreenUpdating = true;
                    // Запрашивать сохранение
                    wdApp.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;
                    //Создаём файл

                    doc.SaveAs(Directory.GetCurrentDirectory() + @"\Приказы\" + @"Неуспевающие " + kurs + @" курс",
                        Word.WdSaveFormat.wdFormatDocument97);
                    doc.Close();

                    count++;
                }
            }
            catch (Exception z)
            {
                MessageBox.Show("Покрашились " + z.Message);
            }
            finally
            {
                wdApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(wdApp);
                doc = null;
                wdApp = null;
                GC.Collect();
            }
        }

        private void Итоги()
        {
            int k, x, y, otd, procent, tk;
            int tx1, tx2, tx6, x1, x2, x6;
            Single tx3, tx5, tx7, x3, x5, x7;


            //Работа с файлами
            //Проверка наличия директории - если её нет, то создаём
            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Приказы\"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Приказы\");

            var t1 = year - 2000;
            var t2 = 0;
            if (month > 0 && month < 4) t2 = -1;
            else if (month > 3 && month < 9) t2 = 0;
            else if (month > 8 && month < 11) t2 = 1;
            else if (month > 10 && month < 13) t2 = 2;

            int kn;
            if ((month > 0 && month < 4) || month == 9 || month == 10) kn = 1;
            else kn = 2;


            //Проп_Сейч
            var strSql = String.Format("SELECT Студенты.КодСт, Успеваемость.Пропуск_уваж, Успеваемость.Пропуск_неуваж" +
                                   " FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент" +
                                   " WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1,
                t2);
            var propNowDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var propNow = new DataTable();
            propNowDt.Fill(propNow);


            //Проп_Пред
            strSql =
                String.Format(
                    "SELECT Студенты.КодСт, [Пропуск_уваж]*{2} AS Проп_Пред_Ув, [Пропуск_неуваж]*{2} AS Проп_Пред_Неув " +
                    "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент " +
                    "WHERE ((Успеваемость.№КН) = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1})", t1, t2 - kn + 1,
                    kn - 1);
            var propDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var prop = new DataTable();
            propDt.Fill(prop);

            /*var propski = (from propNowRow in propNow.AsEnumerable()
                join propRow in prop.AsEnumerable() 
                           on propNowRow.Field<int>("КодСт") 
                           equals propRow.Field<int>("КодСт") 
                           select new
                           {
                               
                           }
            
            ) */




            // Студенты
            strSql =
                @"SELECT (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9) AS Курс, Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"FROM Уч_Отделы INNER JOIN (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) ON Уч_Отделы.КодУч = Группы.Уч_Отдел " +
                @"GROUP BY Студенты.КодСт, Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО " +
                @"ORDER BY (Right(Date(),2)-Right((Группы.Группа),2))+(Month(Date())\9), Уч_Отделы.Уч_Отдел, Группы.Группа, Студенты.Фамилия, Студенты.ИО";
            var studDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var stud = new DataTable();
            studDt.Fill(stud);

            // Средний балл
            strSql = String.Format(
                "SELECT Балл_КН.КодСт, Балл_КН.Балл " +
                "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт " +
                "WHERE (((Балл_КН.[№КН])=4*({0}-Right(([Группы].[Группа]),2))+{1})) " +
                "ORDER BY Балл_КН.КодСт", t1, t2);
            var avgDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var avg = new DataTable();
            avgDt.Fill(avg);

            var avgball = (from avgRow in avg.AsEnumerable()
                           group avgRow by avgRow.ItemArray[0]
                               into grouping
                               orderby grouping.Key
                               select new
                               {
                                   КодСт = grouping.Key,
                                   avgb = grouping.Average(xx => Convert.ToInt32(xx.ItemArray[1]))
                               }).CopyToDataTable();






            //Параметры для взысканий
            var rsPVzDt = new OleDbDataAdapter(new OleDbCommand("SELECT * FROM Парам_Взыск", _connection));
            var rsPVz = new DataTable();
            rsPVzDt.Fill(rsPVz);
            var rs0 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[0]);
            var rs1 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[1]);
            var rs2 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[2]);
            var rs3 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[3]);
            var rs4 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[4]);
            var rs5 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[5]);
            var rs6 = Convert.ToDouble(rsPVz.Rows[0].ItemArray[6]);
            var rs7 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[7]);
            var rs8 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[8]);
            var rs9 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[9]);
            var rs10 = Convert.ToInt32(rsPVz.Rows[0].ItemArray[10]);

            DataTable rs = null;
            if (kn == 1)
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") <= rs0) ||
                             (propRow.Field<int>("Пропуск_неуваж") >= rs3))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propRow.Field<int>("Пропуск_неуваж")
                      }).CopyToDataTable();
            }
            else
            {
                rs = (from studRow in stud.AsEnumerable()
                      join avgRow in avgball.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals avgRow.Field<int>("КодСт")
                      join propNowRow in propNow.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propNowRow.Field<int>("КодСт")
                      join propRow in prop.AsEnumerable()
                          on studRow.Field<int>("КодСт")
                          equals propRow.Field<int>("КодСт")
                      where ((avgRow.Field<Double>("avgb") <= rs0) ||
                             (propNowRow.Field<int>("Пропуск_неуваж") >= rs7))
                      select new
                      {
                          Курс = studRow.Field<double>("Курс"),
                          Уч_Отдел = studRow.Field<string>("Уч_Отдел"),
                          Группа = studRow.Field<string>("Группа"),
                          Фамилия = studRow.Field<string>("Фамилия"),
                          ИО = studRow.Field<string>("ИО"),
                          avgBall = avgRow.Field<double>("avgb"),
                          Пропуск_уваж = propNowRow.Field<int>("Пропуск_уваж"),
                          Пропуск_неуваж = propNowRow.Field<int>("Пропуск_неуваж"),
                          Проп_Пред_Ув = propRow.Field<int>("Проп_Пред_Ув"),
                          Проп_Пред_Неув = propRow.Field<int>("Проп_Пред_Неув")
                      }).CopyToDataTable();
            }

            if (rs.Rows.Count == 0)
            {
                MessageBox.Show(@"Неуспевающих студентов нет!");
                return;
            }
            pBar.Maximum = rs.Rows.Count;
            pBar.Value = 0;

            /*var wdApp = new Word.Application();
            Word.Document doc;
            Word.Range rngCur;
            try
            {

            }
            finally
            {
                
            }*/
            /*
    
        
    'rs.Open "Итоги_Пропусков", , adOpenStatic, , adCmdTable
    Set rs = db.OpenRecordset("Итоги_Пропусков", dbOpenSnapshot, dbOpenTable)  'закрыть
    
    If rs.EOF Then
        rs.Close
        db.Close
        ProgrBar.Visible = False
        DoCmd.Hourglass False   'Убрать песочные часы
        MsgBox "Данные о итогах отсутствуют !!!"
        Exit Sub
    End If
    rs.MoveLast
    k = rs.RecordCount
    rs.MoveFirst
    ProgrBar.Min = 0
    ProgrBar.Max = 2 * k
    ProgrBar.Visible = True
    
    'Set rs2.ActiveConnection = db
    'rs2.Open "Итоги_Предметов", , adOpenStatic, , adCmdTable
    Set rs2 = db.OpenRecordset("Итоги_Предметов", dbOpenSnapshot, dbOpenTable)  'закрыть
    rs2.MoveFirst
    
    'Set rs3.ActiveConnection = db
    'rs3.Open "Итоги_Баллы", , adOpenStatic, , adCmdTable
    Set rs3 = db.OpenRecordset("Итоги_Баллы", dbOpenSnapshot, dbOpenTable)      'закрыть
    rs3.MoveFirst
    
    'Set rs4.ActiveConnection = db
    'rs4.Open "Итоги_С_Нулями", , adOpenStatic, , adCmdTable
    Set rs4 = db.OpenRecordset("Итоги_С_Нулями", dbOpenSnapshot, dbOpenTable)   'закрыть
    rs4.MoveFirst
    
    Do Until rs.EOF
        otd = rs(0)
        Set appWord = New Word.Application
        Set wExport = GetObject(CurrentProject.Path & "\Оформление приказа\" & "Бланк_Итоги.doc")
        Set rngCurrent = wExport.Content
        appWord.Visible = True
        With rngCurrent
            '№ КН
            .Find.Text = "{А_№КН}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1, 2, 3, 9, 10
                .Text = "1"
            Case Else
                .Text = "2"
            End Select
            'Семестр
            .Find.Text = "{А_Сем}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1 To 8
                .Text = "весеннего"
            Case Else
                .Text = "осеннего"
            End Select
            'Учебные года
            .Find.Text = "{А_Года}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1 To 8
                .Text = (Year(Now()) - 1) & "/" & Year(Now())
            Case Else
                .Text = Year(Now()) & "/" & (Year(Now()) + 1)
            End Select
        End With
        Set myTable = wExport.Tables(1)
        myTable.Range.Rows.Add
        myTable.Cell(3, 1).Range.Text = "Курс 1"
        x = 4
        y = 1
        tk = 0
        tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
        x1 = 0: x2 = 0: x3 = 0: x5 = 0: x6 = 0: x7 = 0
        Do Until rs(0) <> otd
            If rs(1) <> y Then
                y = rs(1)
                tx3 = tx2 / tx1
                tx5 = tx5 / tk
                tx7 = (tx1 - tx6) / tx1 * 100
                myTable.Range.Rows.Add
                myTable.Cell(x, 1).Range.Text = "Итого:"
                myTable.Cell(x, 2).Range.Text = tx1
                myTable.Cell(x, 3).Range.Text = tx2
                myTable.Cell(x, 4).Range.Text = Round(tx3, 2)
                myTable.Cell(x, 5).Range.Text = "-"
                myTable.Cell(x, 6).Range.Text = Round(tx5, 2)
                myTable.Cell(x, 7).Range.Text = tx6
                myTable.Cell(x, 8).Range.Text = Round(tx7, 2)
                myTable.Range.Rows.Add
                myTable.Cell(x + 1, 1).Range.Text = "Курс " & y
                x = x + 2
                x1 = x1 + tx1: x2 = x2 + tx2
                x5 = x5 + tx5: x6 = x6 + tx6
                tk = 0: tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
            End If
            myTable.Range.Rows.Add
            myTable.Cell(x, 1).Range.Text = rs(2)            'Название группы
            myTable.Cell(x, 2).Range.Text = rs(3)            'Число студентов
            tx1 = tx1 + rs(3)
            myTable.Cell(x, 3).Range.Text = rs(4)            'Пропусков всего
            tx2 = tx2 + rs(4)
            myTable.Cell(x, 4).Range.Text = Round(rs(5), 2)  'Пропусков на одного
            myTable.Cell(x, 5).Range.Text = rs2(0)           'Дисциплин
            'If rs3(0) Is Not Null Then
            If rs3.EOF <> True Then
                myTable.Cell(x, 6).Range.Text = Round(rs3(0), 2) 'Средний балл
                tx5 = tx5 + Round(rs3(0), 2)
            End If
            'Результат запроса "Итоги_С_Нулями" содержит сведения только о группах с процентом успеваемости <100
            If rs4(1) = rs(2) Then
                myTable.Cell(x, 7).Range.Text = rs4(0)       'Количество студентов с "0"
                tx6 = tx6 + rs4(0)
                myTable.Cell(x, 8).Range.Text = Round(((rs(3) - rs4(0)) / rs(3) * 100), 2) 'Процент успеваемости
                tk = tk + 1
                x = x + 1
                ProgrBar.Value = rs.AbsolutePosition
                rs.MoveNext
                rs2.MoveNext
                If rs3.EOF <> True Then
                    rs3.MoveNext
                End If
                rs4.MoveNext
            Else                                             'В группе нет студентов с "0"
                myTable.Cell(x, 7).Range.Text = "0"          'Количество студентов с "0"
                tx6 = tx6
                myTable.Cell(x, 8).Range.Text = "100"        'Процент успеваемости
                tk = tk + 1
                x = x + 1
                ProgrBar.Value = rs.AbsolutePosition
                rs.MoveNext
                rs2.MoveNext
                rs3.MoveNext
            End If
            If rs.EOF Then
                Exit Do
            End If
        Loop
        tx3 = tx2 / tx1
        tx5 = tx5 / tk
        tx7 = (tx1 - tx6) / tx1 * 100
        myTable.Range.Rows.Add
        myTable.Cell(x, 1).Range.Text = "Итого:"
        myTable.Cell(x, 2).Range.Text = tx1
        myTable.Cell(x, 3).Range.Text = tx2
        myTable.Cell(x, 4).Range.Text = Round(tx3, 2)
        myTable.Cell(x, 5).Range.Text = "-"
        myTable.Cell(x, 6).Range.Text = Round(tx5, 2)
        myTable.Cell(x, 7).Range.Text = tx6
        myTable.Cell(x, 8).Range.Text = Round(tx7, 2)
        x = x + 1
        x1 = x1 + tx1: x2 = x2 + tx2
        x5 = x5 + tx5: x6 = x6 + tx6
        tk = 0: tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
        x3 = x2 / x1
        x5 = x5 / 4     '4 - число курсов
        x7 = (x1 - x6) / x1 * 100
        myTable.Range.Rows.Add
        myTable.Cell(x, 1).Range.Text = "Итого по факту:"
        myTable.Cell(x, 2).Range.Text = x1
        myTable.Cell(x, 3).Range.Text = x2
        myTable.Cell(x, 4).Range.Text = Round(x3, 2)
        myTable.Cell(x, 5).Range.Text = "-"
        myTable.Cell(x, 6).Range.Text = Round(x5, 2)
        myTable.Cell(x, 7).Range.Text = x6
        myTable.Cell(x, 8).Range.Text = Round(x7, 2)
        x1 = 0: x2 = 0: x3 = 0: x5 = 0: x6 = 0: x7 = 0
        'Если файл существует, то его удалаяем
        If Dir(CurrentProject.Path & "\Приказы\" & "Итоги" & otd & ".doc") <> "" Then
            Kill CurrentProject.Path & "\Приказы\" & "Итоги" & otd & ".doc"
        End If
        wExport.SaveAs (CurrentProject.Path & "\Приказы\" & "Итоги" & otd & ".doc")
        wExport.Close
        Set wExport = Nothing
        appWord.Visible = False
        appWord.Quit
        Set appWord = Nothing
    Loop
    rs.Close
    rs2.Close
    rs3.Close
    rs4.Close
    
    
    'Подведение общих итогов
    'Set rs.ActiveConnection = db
    'rs.Open "Итоги_Пропусков_Общий", , adOpenStatic, , adCmdTable
    Set rs = db.OpenRecordset("Итоги_Пропусков_Общий", dbOpenSnapshot, dbOpenTable)  'закрыть
    rs.MoveFirst
    
    'Set rs2.ActiveConnection = db
    'rs2.Open "Итоги_Предметов_Общий", , adOpenStatic, , adCmdTable
    Set rs2 = db.OpenRecordset("Итоги_Предметов_Общий", dbOpenSnapshot, dbOpenTable)  'закрыть
    rs2.MoveFirst
    
    'Set rs3.ActiveConnection = db
    'rs3.Open "Итоги_Баллы_Общий", , adOpenStatic, , adCmdTable
    Set rs3 = db.OpenRecordset("Итоги_Баллы_Общий", dbOpenSnapshot, dbOpenTable)  'закрыть
    rs3.MoveFirst
    
    'Set rs4.ActiveConnection = db
    'rs4.Open "Итоги_С_Нулями_Общий", , adOpenStatic, , adCmdTable
    Set rs4 = db.OpenRecordset("Итоги_С_Нулями_Общий", dbOpenSnapshot, dbOpenTable) 'закрыть
    rs4.MoveFirst
    
        Set appWord = New Word.Application
        Set wExport = GetObject(CurrentProject.Path & "\Оформление приказа\" & "Бланк_Итоги.doc")
        Set rngCurrent = wExport.Content
        appWord.Visible = True
        With rngCurrent
            '№ КН
            .Find.Text = "{А_№КН}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1, 2, 3, 9, 10
                .Text = "1"
            Case Else
                .Text = "2"
            End Select
            'Семестр
            .Find.Text = "{А_Сем}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1 To 8
                .Text = "весеннего"
            Case Else
                .Text = "осеннего"
            End Select
            'Учебные года
            .Find.Text = "{А_Года}"
            .Find.Forward = True
            .Find.Wrap = wdFindContinue
            .Find.Execute
            Select Case Month(Now())
            Case 1 To 8
                .Text = (Year(Now()) - 1) & "/" & Year(Now())
            Case Else
                .Text = Year(Now()) & "/" & (Year(Now()) + 1)
            End Select
        End With
        Set myTable = wExport.Tables(1)
        myTable.Range.Rows.Add
        myTable.Cell(3, 1).Range.Text = "Курс 1"
        x = 4
        y = 1
        tk = 0
        tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
        x1 = 0: x2 = 0: x3 = 0: x5 = 0: x6 = 0: x7 = 0
        Do Until rs.EOF
            If rs(1) <> y Then
                y = rs(1)
                tx3 = tx2 / tx1
                tx5 = tx5 / tk
                tx7 = (tx1 - tx6) / tx1 * 100
                myTable.Range.Rows.Add
                myTable.Cell(x, 1).Range.Text = "Итого:"
                myTable.Cell(x, 2).Range.Text = tx1
                myTable.Cell(x, 3).Range.Text = tx2
                myTable.Cell(x, 4).Range.Text = Round(tx3, 2)
                myTable.Cell(x, 5).Range.Text = "-"
                myTable.Cell(x, 6).Range.Text = Round(tx5, 2)
                myTable.Cell(x, 7).Range.Text = tx6
                myTable.Cell(x, 8).Range.Text = Round(tx7, 2)
                myTable.Range.Rows.Add
                myTable.Cell(x + 1, 1).Range.Text = "Курс " & y
                x = x + 2
                x1 = x1 + tx1: x2 = x2 + tx2
                x5 = x5 + tx5: x6 = x6 + tx6
                tk = 0: tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
            End If
            myTable.Range.Rows.Add
            myTable.Cell(x, 1).Range.Text = rs(2)            'Название группы
            myTable.Cell(x, 2).Range.Text = rs(3)            'Число студентов
            tx1 = tx1 + rs(3)
            myTable.Cell(x, 3).Range.Text = rs(4)            'Пропусков всего
            tx2 = tx2 + rs(4)
            myTable.Cell(x, 4).Range.Text = Round(rs(5), 2)  'Пропусков на одного
            myTable.Cell(x, 5).Range.Text = rs2(0)           'Дисциплин
            If rs3.EOF <> True Then
                myTable.Cell(x, 6).Range.Text = Round(rs3(0), 2) 'Средний балл
                tx5 = tx5 + Round(rs3(0), 2)
            End If
            'Результат запроса "Итоги_С_Нулями_Общий" содержит сведения только о группах с процентом успеваемости <100
            If rs4(1) = rs(2) Then
                myTable.Cell(x, 7).Range.Text = rs4(0)       'Количество студентов с "0"
                tx6 = tx6 + rs4(0)
                myTable.Cell(x, 8).Range.Text = Round(((rs(3) - rs4(0)) / rs(3) * 100), 2) 'Процент успеваемости
                tk = tk + 1
                x = x + 1
                ProgrBar.Value = k + rs.AbsolutePosition
                rs.MoveNext
                rs2.MoveNext
                If rs3.EOF <> True Then
                    rs3.MoveNext
                End If
                rs4.MoveNext
            Else
                myTable.Cell(x, 7).Range.Text = "0"          'Количество студентов с "0"
                tx6 = tx6
                myTable.Cell(x, 8).Range.Text = "100"        'Процент успеваемости
                tk = tk + 1
                x = x + 1
                ProgrBar.Value = k + rs.AbsolutePosition
                rs.MoveNext
                rs2.MoveNext
                rs3.MoveNext
            End If
            If rs.EOF Then
                Exit Do
            End If
        Loop
        tx3 = tx2 / tx1
        tx5 = tx5 / tk
        tx7 = (tx1 - tx6) / tx1 * 100
        myTable.Range.Rows.Add
        myTable.Cell(x, 1).Range.Text = "Итого:"
        myTable.Cell(x, 2).Range.Text = tx1
        myTable.Cell(x, 3).Range.Text = tx2
        myTable.Cell(x, 4).Range.Text = Round(tx3, 2)
        myTable.Cell(x, 5).Range.Text = "-"
        myTable.Cell(x, 6).Range.Text = Round(tx5, 2)
        myTable.Cell(x, 7).Range.Text = tx6
        myTable.Cell(x, 8).Range.Text = Round(tx7, 2)
        x = x + 1
        x1 = x1 + tx1: x2 = x2 + tx2
        x5 = x5 + tx5: x6 = x6 + tx6
        tk = 0: tx1 = 0: tx2 = 0: tx3 = 0: tx5 = 0: tx6 = 0: tx7 = 0
        x3 = x2 / x1
        x5 = x5 / 4     '4 - число курсов
        x7 = (x1 - x6) / x1 * 100
        myTable.Range.Rows.Add
        myTable.Cell(x, 1).Range.Text = "Итого по факту:"
        myTable.Cell(x, 2).Range.Text = x1
        myTable.Cell(x, 3).Range.Text = x2
        myTable.Cell(x, 4).Range.Text = Round(x3, 2)
        myTable.Cell(x, 5).Range.Text = "-"
        myTable.Cell(x, 6).Range.Text = Round(x5, 2)
        myTable.Cell(x, 7).Range.Text = x6
        myTable.Cell(x, 8).Range.Text = Round(x7, 2)
        x1 = 0: x2 = 0: x3 = 0: x5 = 0: x6 = 0: x7 = 0
        'Если файл существует, то его удалаяем
        If Dir(CurrentProject.Path & "\Приказы\" & "Итоги.doc") <> "" Then
            Kill CurrentProject.Path & "\Приказы\" & "Итоги.doc"
        End If
        wExport.SaveAs (CurrentProject.Path & "\Приказы\" & "Итоги.doc")
        wExport.Close
        Set wExport = Nothing
        appWord.Visible = False
        appWord.Quit
        Set appWord = Nothing
    
    DoCmd.Hourglass False   'Убрать песочные часы
    rs.Close
    rs2.Close
    rs3.Close
    rs4.Close
    
    db.Close
    ProgrBar.Visible = False
End Sub

             */
        }
    }

    public class ObjectShredder<T>
    {
        private System.Reflection.FieldInfo[] _fi;
        private System.Reflection.PropertyInfo[] _pi;
        private System.Collections.Generic.Dictionary<string, int> _ordinalMap;
        private System.Type _type;

        // ObjectShredder constructor.
        public ObjectShredder()
        {
            _type = typeof(T);
            _fi = _type.GetFields();
            _pi = _type.GetProperties();
            _ordinalMap = new Dictionary<string, int>();
        }

        /// <summary>
        /// Loads a DataTable from a sequence of objects.
        /// </summary>
        /// <param name="source">The sequence of objects to load into the DataTable.</param>
        /// <param name="table">The input table. The schema of the table must match that 
        /// the type T.  If the table is null, a new table is created with a schema 
        /// created from the public properties and fields of the type T.</param>
        /// <param name="options">Specifies how values from the source sequence will be applied to 
        /// existing rows in the table.</param>
        /// <returns>A DataTable created from the source sequence.</returns>
        public DataTable Shred(IEnumerable<T> source, DataTable table, LoadOption? options)
        {
            // Load the table from the scalar sequence if T is a primitive type.
            if (typeof(T).IsPrimitive)
            {
                return ShredPrimitive(source, table, options);
            }

            // Create a new table if the input table is null.
            if (table == null)
            {
                table = new DataTable(typeof(T).Name);
            }

            // Initialize the ordinal map and extend the table schema based on type T.
            table = ExtendTable(table, typeof(T));

            // Enumerate the source sequence and load the object values into rows.
            table.BeginLoadData();
            using (IEnumerator<T> e = source.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    if (options != null)
                    {
                        table.LoadDataRow(ShredObject(table, e.Current), (LoadOption)options);
                    }
                    else
                    {
                        table.LoadDataRow(ShredObject(table, e.Current), true);
                    }
                }
            }
            table.EndLoadData();

            // Return the table.
            return table;
        }

        public DataTable ShredPrimitive(IEnumerable<T> source, DataTable table, LoadOption? options)
        {
            // Create a new table if the input table is null.
            if (table == null)
            {
                table = new DataTable(typeof(T).Name);
            }

            if (!table.Columns.Contains("Value"))
            {
                table.Columns.Add("Value", typeof(T));
            }

            // Enumerate the source sequence and load the scalar values into rows.
            table.BeginLoadData();
            using (IEnumerator<T> e = source.GetEnumerator())
            {
                Object[] values = new object[table.Columns.Count];
                while (e.MoveNext())
                {
                    values[table.Columns["Value"].Ordinal] = e.Current;

                    if (options != null)
                    {
                        table.LoadDataRow(values, (LoadOption)options);
                    }
                    else
                    {
                        table.LoadDataRow(values, true);
                    }
                }
            }
            table.EndLoadData();

            // Return the table.
            return table;
        }

        public object[] ShredObject(DataTable table, T instance)
        {

            FieldInfo[] fi = _fi;
            PropertyInfo[] pi = _pi;

            if (instance.GetType() != typeof(T))
            {
                // If the instance is derived from T, extend the table schema
                // and get the properties and fields.
                ExtendTable(table, instance.GetType());
                fi = instance.GetType().GetFields();
                pi = instance.GetType().GetProperties();
            }

            // Add the property and field values of the instance to an array.
            Object[] values = new object[table.Columns.Count];
            foreach (FieldInfo f in fi)
            {
                values[_ordinalMap[f.Name]] = f.GetValue(instance);
            }

            foreach (PropertyInfo p in pi)
            {
                values[_ordinalMap[p.Name]] = p.GetValue(instance, null);
            }

            // Return the property and field values of the instance.
            return values;
        }

        public DataTable ExtendTable(DataTable table, Type type)
        {
            // Extend the table schema if the input table was null or if the value 
            // in the sequence is derived from type T.            
            foreach (FieldInfo f in type.GetFields())
            {
                if (!_ordinalMap.ContainsKey(f.Name))
                {
                    // Add the field as a column in the table if it doesn't exist
                    // already.
                    DataColumn dc = table.Columns.Contains(f.Name) ? table.Columns[f.Name]
                        : table.Columns.Add(f.Name, f.FieldType);

                    // Add the field to the ordinal map.
                    _ordinalMap.Add(f.Name, dc.Ordinal);
                }
            }
            foreach (PropertyInfo p in type.GetProperties())
            {
                if (!_ordinalMap.ContainsKey(p.Name))
                {
                    // Add the property as a column in the table if it doesn't exist
                    // already.
                    DataColumn dc = table.Columns.Contains(p.Name) ? table.Columns[p.Name]
                        : table.Columns.Add(p.Name, p.PropertyType);

                    // Add the property to the ordinal map.
                    _ordinalMap.Add(p.Name, dc.Ordinal);
                }
            }

            // Return the table.
            return table;
        }
    }

    public static class CustomLINQtoDataSetMethods
    {
        public static DataTable CopyToDataTable<T>(this IEnumerable<T> source)
        {
            return new ObjectShredder<T>().Shred(source, null, null);
        }

        public static DataTable CopyToDataTable<T>(this IEnumerable<T> source,
                                                    DataTable table, LoadOption? options)
        {
            return new ObjectShredder<T>().Shred(source, table, options);
        }
    }
}
