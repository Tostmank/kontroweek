﻿namespace KontroWeek
{
    partial class LoadData
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.pBar = new System.Windows.Forms.ProgressBar();
            this.cB_Group = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.учОтделыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._1DataSet = new KontroWeek._1DataSet();
            this.уч_ОтделыTableAdapter = new KontroWeek._1DataSetTableAdapters.Уч_ОтделыTableAdapter();
            this.учОтделыГруппыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.группыTableAdapter = new KontroWeek._1DataSetTableAdapters.ГруппыTableAdapter();
            this.группыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.учОтделыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.учОтделыГруппыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.группыBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(130, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Создать бланки";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pBar
            // 
            this.pBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pBar.Location = new System.Drawing.Point(12, 335);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(233, 23);
            this.pBar.TabIndex = 2;
            // 
            // cB_Group
            // 
            this.cB_Group.AutoSize = true;
            this.cB_Group.Checked = true;
            this.cB_Group.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cB_Group.Location = new System.Drawing.Point(164, 12);
            this.cB_Group.Name = "cB_Group";
            this.cB_Group.Size = new System.Drawing.Size(45, 17);
            this.cB_Group.TabIndex = 3;
            this.cB_Group.Text = "Все";
            this.cB_Group.UseVisualStyleBackColor = true;
            this.cB_Group.CheckedChanged += new System.EventHandler(this.cB_Group_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(130, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Загрузить бланки";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // учОтделыBindingSource
            // 
            this.учОтделыBindingSource.DataMember = "Уч_Отделы";
            this.учОтделыBindingSource.DataSource = this._1DataSet;
            // 
            // _1DataSet
            // 
            this._1DataSet.DataSetName = "_1DataSet";
            this._1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // уч_ОтделыTableAdapter
            // 
            this.уч_ОтделыTableAdapter.ClearBeforeFill = true;
            // 
            // учОтделыГруппыBindingSource
            // 
            this.учОтделыГруппыBindingSource.DataMember = "Уч_ОтделыГруппы";
            this.учОтделыГруппыBindingSource.DataSource = this.учОтделыBindingSource;
            // 
            // группыTableAdapter
            // 
            this.группыTableAdapter.ClearBeforeFill = true;
            // 
            // группыBindingSource
            // 
            this.группыBindingSource.DataMember = "Группы";
            this.группыBindingSource.DataSource = this._1DataSet;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.Enabled = false;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(112, 303);
            this.listBox1.TabIndex = 13;
            // 
            // LoadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 370);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cB_Group);
            this.Controls.Add(this.pBar);
            this.Controls.Add(this.button1);
            this.Name = "LoadData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Работа с Excel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadData_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.учОтделыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.учОтделыГруппыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.группыBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar pBar;
        private System.Windows.Forms.CheckBox cB_Group;
        private System.Windows.Forms.Button button2;
        private _1DataSet _1DataSet;
        private System.Windows.Forms.BindingSource учОтделыBindingSource;
        private _1DataSetTableAdapters.Уч_ОтделыTableAdapter уч_ОтделыTableAdapter;
        private System.Windows.Forms.BindingSource учОтделыГруппыBindingSource;
        private _1DataSetTableAdapters.ГруппыTableAdapter группыTableAdapter;
        private System.Windows.Forms.BindingSource группыBindingSource;
        private System.Windows.Forms.ListBox listBox1;
    }
}

