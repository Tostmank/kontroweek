﻿namespace KontroWeek
{
    partial class Kurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblGrps = new System.Windows.Forms.Label();
            this.lblStds = new System.Windows.Forms.Label();
            this.bAddGr = new System.Windows.Forms.Button();
            this.группыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._1DataSet = new KontroWeek._1DataSet();
            this.группыTableAdapter = new KontroWeek._1DataSetTableAdapters.ГруппыTableAdapter();
            this.bDelGr = new System.Windows.Forms.Button();
            this.bRedGr = new System.Windows.Forms.Button();
            this.студентыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.студентыTableAdapter = new KontroWeek._1DataSetTableAdapters.СтудентыTableAdapter();
            this.delSt = new System.Windows.Forms.Button();
            this.adSt = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.предметыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.предмГрСеместрBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.предм_Гр_СеместрTableAdapter = new KontroWeek._1DataSetTableAdapters.Предм_Гр_СеместрTableAdapter();
            this.предметыTableAdapter = new KontroWeek._1DataSetTableAdapters.ПредметыTableAdapter();
            this.delPre = new System.Windows.Forms.Button();
            this.aqCh = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cbAllPredms = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgPr = new KontroWeek.NDGW();
            this.кодПрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.предметDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типПрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.семестрDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgShoPr = new KontroWeek.NDGW();
            this.cbPredm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbRef = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbUpr = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bLb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbRZ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbKR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbKP = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbSem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgGrp = new KontroWeek.NDGW();
            this.кодГрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.учОтделDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgStd = new KontroWeek.NDGW();
            this.кодСтDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.фамилияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.фамилияДDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.иОDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.формаОбучDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.группыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.студентыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.предмГрСеместрBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgShoPr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgGrp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGrps
            // 
            this.lblGrps.AutoSize = true;
            this.lblGrps.Location = new System.Drawing.Point(9, 9);
            this.lblGrps.Name = "lblGrps";
            this.lblGrps.Size = new System.Drawing.Size(44, 13);
            this.lblGrps.TabIndex = 0;
            this.lblGrps.Text = "Группы";
            // 
            // lblStds
            // 
            this.lblStds.AutoSize = true;
            this.lblStds.Location = new System.Drawing.Point(138, 9);
            this.lblStds.Name = "lblStds";
            this.lblStds.Size = new System.Drawing.Size(55, 13);
            this.lblStds.TabIndex = 3;
            this.lblStds.Text = "Студенты";
            // 
            // bAddGr
            // 
            this.bAddGr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAddGr.Location = new System.Drawing.Point(12, 348);
            this.bAddGr.Name = "bAddGr";
            this.bAddGr.Size = new System.Drawing.Size(120, 23);
            this.bAddGr.TabIndex = 7;
            this.bAddGr.Text = "Добавить группу";
            this.bAddGr.UseVisualStyleBackColor = true;
            this.bAddGr.Click += new System.EventHandler(this.bAddGr_Click);
            // 
            // группыBindingSource
            // 
            this.группыBindingSource.DataMember = "Группы";
            this.группыBindingSource.DataSource = this._1DataSet;
            // 
            // _1DataSet
            // 
            this._1DataSet.DataSetName = "_1DataSet";
            this._1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // группыTableAdapter
            // 
            this.группыTableAdapter.ClearBeforeFill = true;
            // 
            // bDelGr
            // 
            this.bDelGr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelGr.Location = new System.Drawing.Point(12, 406);
            this.bDelGr.Name = "bDelGr";
            this.bDelGr.Size = new System.Drawing.Size(120, 23);
            this.bDelGr.TabIndex = 9;
            this.bDelGr.Text = "Удалить группу";
            this.bDelGr.UseVisualStyleBackColor = true;
            this.bDelGr.Click += new System.EventHandler(this.bRemGr_Click);
            // 
            // bRedGr
            // 
            this.bRedGr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bRedGr.Location = new System.Drawing.Point(12, 377);
            this.bRedGr.Name = "bRedGr";
            this.bRedGr.Size = new System.Drawing.Size(120, 23);
            this.bRedGr.TabIndex = 10;
            this.bRedGr.Text = "Редактировать";
            this.bRedGr.UseVisualStyleBackColor = true;
            this.bRedGr.Click += new System.EventHandler(this.bRedGr_Click);
            // 
            // студентыBindingSource
            // 
            this.студентыBindingSource.DataMember = "Студенты";
            this.студентыBindingSource.DataSource = this._1DataSet;
            // 
            // студентыTableAdapter
            // 
            this.студентыTableAdapter.ClearBeforeFill = true;
            // 
            // delSt
            // 
            this.delSt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delSt.Location = new System.Drawing.Point(141, 406);
            this.delSt.Name = "delSt";
            this.delSt.Size = new System.Drawing.Size(165, 23);
            this.delSt.TabIndex = 13;
            this.delSt.Text = "Удалить студента";
            this.delSt.UseVisualStyleBackColor = true;
            this.delSt.Click += new System.EventHandler(this.delSt_Click);
            // 
            // adSt
            // 
            this.adSt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.adSt.Location = new System.Drawing.Point(141, 348);
            this.adSt.Name = "adSt";
            this.adSt.Size = new System.Drawing.Size(165, 23);
            this.adSt.TabIndex = 12;
            this.adSt.Text = "Добавить студента";
            this.adSt.UseVisualStyleBackColor = true;
            this.adSt.Click += new System.EventHandler(this.adSt_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(141, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Перевести в другую группу";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // предметыBindingSource
            // 
            this.предметыBindingSource.DataMember = "Предметы";
            this.предметыBindingSource.DataSource = this._1DataSet;
            this.предметыBindingSource.Sort = "Предмет";
            // 
            // предмГрСеместрBindingSource
            // 
            this.предмГрСеместрBindingSource.DataMember = "Предм_Гр_Семестр";
            this.предмГрСеместрBindingSource.DataSource = this._1DataSet;
            // 
            // предм_Гр_СеместрTableAdapter
            // 
            this.предм_Гр_СеместрTableAdapter.ClearBeforeFill = true;
            // 
            // предметыTableAdapter
            // 
            this.предметыTableAdapter.ClearBeforeFill = true;
            // 
            // delPre
            // 
            this.delPre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delPre.Location = new System.Drawing.Point(617, 406);
            this.delPre.Name = "delPre";
            this.delPre.Size = new System.Drawing.Size(148, 23);
            this.delPre.TabIndex = 17;
            this.delPre.Text = "Удалить предмет";
            this.delPre.UseVisualStyleBackColor = true;
            this.delPre.Click += new System.EventHandler(this.delPre_Click);
            // 
            // aqCh
            // 
            this.aqCh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.aqCh.Location = new System.Drawing.Point(617, 377);
            this.aqCh.Name = "aqCh";
            this.aqCh.Size = new System.Drawing.Size(148, 23);
            this.aqCh.TabIndex = 18;
            this.aqCh.Text = "Принять";
            this.aqCh.UseVisualStyleBackColor = true;
            this.aqCh.Click += new System.EventHandler(this.aqCh_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(617, 348);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Выбрать шаблон";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbAllPredms
            // 
            this.cbAllPredms.AutoSize = true;
            this.cbAllPredms.Location = new System.Drawing.Point(982, 2);
            this.cbAllPredms.Name = "cbAllPredms";
            this.cbAllPredms.Size = new System.Drawing.Size(151, 17);
            this.cbAllPredms.TabIndex = 20;
            this.cbAllPredms.Text = "Выводить все семестры";
            this.cbAllPredms.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(771, 382);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "<- ЭТУ ЧУДЕСНУЮ КНОПОЧКУ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(416, 382);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "НЕ ЗАБЫВАЙТЕ НАЖИМАТЬ ->";
            // 
            // dgPr
            // 
            this.dgPr.AllowUserToAddRows = false;
            this.dgPr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPr.AutoGenerateColumns = false;
            this.dgPr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодПрDataGridViewTextBoxColumn,
            this.предметDataGridViewTextBoxColumn,
            this.типПрDataGridViewTextBoxColumn,
            this.семестрDataGridViewTextBoxColumn1,
            this.группаDataGridViewTextBoxColumn2});
            this.dgPr.DataSource = this.предмГрСеместрBindingSource;
            this.dgPr.Location = new System.Drawing.Point(546, 348);
            this.dgPr.Name = "dgPr";
            this.dgPr.Size = new System.Drawing.Size(65, 31);
            this.dgPr.TabIndex = 16;
            this.dgPr.Visible = false;
            this.dgPr.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPr_CellContentClick);
            // 
            // кодПрDataGridViewTextBoxColumn
            // 
            this.кодПрDataGridViewTextBoxColumn.DataPropertyName = "КодПр";
            this.кодПрDataGridViewTextBoxColumn.HeaderText = "КодПр";
            this.кодПрDataGridViewTextBoxColumn.Name = "кодПрDataGridViewTextBoxColumn";
            // 
            // предметDataGridViewTextBoxColumn
            // 
            this.предметDataGridViewTextBoxColumn.DataPropertyName = "Предмет";
            this.предметDataGridViewTextBoxColumn.HeaderText = "Предмет";
            this.предметDataGridViewTextBoxColumn.Name = "предметDataGridViewTextBoxColumn";
            // 
            // типПрDataGridViewTextBoxColumn
            // 
            this.типПрDataGridViewTextBoxColumn.DataPropertyName = "ТипПр";
            this.типПрDataGridViewTextBoxColumn.HeaderText = "ТипПр";
            this.типПрDataGridViewTextBoxColumn.Name = "типПрDataGridViewTextBoxColumn";
            // 
            // семестрDataGridViewTextBoxColumn1
            // 
            this.семестрDataGridViewTextBoxColumn1.DataPropertyName = "Семестр";
            this.семестрDataGridViewTextBoxColumn1.HeaderText = "Семестр";
            this.семестрDataGridViewTextBoxColumn1.Name = "семестрDataGridViewTextBoxColumn1";
            // 
            // группаDataGridViewTextBoxColumn2
            // 
            this.группаDataGridViewTextBoxColumn2.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn2.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn2.Name = "группаDataGridViewTextBoxColumn2";
            // 
            // dgShoPr
            // 
            this.dgShoPr.AllowUserToResizeRows = false;
            this.dgShoPr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgShoPr.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgShoPr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgShoPr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbPredm,
            this.cbRef,
            this.cbUpr,
            this.bLb,
            this.cbRZ,
            this.cbKR,
            this.cbKP,
            this.cbSem});
            this.dgShoPr.Location = new System.Drawing.Point(617, 25);
            this.dgShoPr.Name = "dgShoPr";
            this.dgShoPr.RowHeadersVisible = false;
            this.dgShoPr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgShoPr.Size = new System.Drawing.Size(516, 317);
            this.dgShoPr.TabIndex = 15;
            this.dgShoPr.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgShoPr_CellClick);
            this.dgShoPr.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgShoPr_CellContentClick);
            this.dgShoPr.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ndgw1_CellValueChanged);
            this.dgShoPr.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgShoPr_RowsAdded);
            // 
            // cbPredm
            // 
            this.cbPredm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cbPredm.DataSource = this.предметыBindingSource;
            this.cbPredm.DisplayMember = "Предмет";
            this.cbPredm.HeaderText = "Предмет";
            this.cbPredm.MinimumWidth = 200;
            this.cbPredm.Name = "cbPredm";
            this.cbPredm.ValueMember = "КодПр";
            // 
            // cbRef
            // 
            this.cbRef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.cbRef.FalseValue = "Nope";
            this.cbRef.HeaderText = "РЕФ";
            this.cbRef.IndeterminateValue = "Nope";
            this.cbRef.Name = "cbRef";
            this.cbRef.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbRef.TrueValue = "True";
            this.cbRef.Width = 38;
            // 
            // cbUpr
            // 
            this.cbUpr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.cbUpr.FalseValue = "Nope";
            this.cbUpr.HeaderText = "УПР";
            this.cbUpr.IndeterminateValue = "Nope";
            this.cbUpr.Name = "cbUpr";
            this.cbUpr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbUpr.TrueValue = "True";
            this.cbUpr.Width = 36;
            // 
            // bLb
            // 
            this.bLb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.bLb.FalseValue = "Nope";
            this.bLb.HeaderText = "ЛБ";
            this.bLb.IndeterminateValue = "Nope";
            this.bLb.Name = "bLb";
            this.bLb.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bLb.TrueValue = "True";
            this.bLb.Width = 28;
            // 
            // cbRZ
            // 
            this.cbRZ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.cbRZ.FalseValue = "Nope";
            this.cbRZ.HeaderText = "РЗ";
            this.cbRZ.IndeterminateValue = "Nope";
            this.cbRZ.Name = "cbRZ";
            this.cbRZ.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbRZ.TrueValue = "True";
            this.cbRZ.Width = 27;
            // 
            // cbKR
            // 
            this.cbKR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.cbKR.FalseValue = "Nope";
            this.cbKR.HeaderText = "КР";
            this.cbKR.IndeterminateValue = "Nope";
            this.cbKR.Name = "cbKR";
            this.cbKR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbKR.TrueValue = "True";
            this.cbKR.Width = 27;
            // 
            // cbKP
            // 
            this.cbKP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.cbKP.FalseValue = "Nope";
            this.cbKP.HeaderText = "КП";
            this.cbKP.IndeterminateValue = "Nope";
            this.cbKP.Name = "cbKP";
            this.cbKP.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbKP.TrueValue = "True";
            this.cbKP.Width = 28;
            // 
            // cbSem
            // 
            this.cbSem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cbSem.HeaderText = "Семестр";
            this.cbSem.Name = "cbSem";
            this.cbSem.Width = 76;
            // 
            // dgGrp
            // 
            this.dgGrp.AllowUserToAddRows = false;
            this.dgGrp.AllowUserToResizeColumns = false;
            this.dgGrp.AllowUserToResizeRows = false;
            this.dgGrp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgGrp.AutoGenerateColumns = false;
            this.dgGrp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgGrp.ColumnHeadersVisible = false;
            this.dgGrp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодГрDataGridViewTextBoxColumn,
            this.группаDataGridViewTextBoxColumn,
            this.учОтделDataGridViewTextBoxColumn});
            this.dgGrp.DataSource = this.группыBindingSource;
            this.dgGrp.Location = new System.Drawing.Point(12, 25);
            this.dgGrp.MultiSelect = false;
            this.dgGrp.Name = "dgGrp";
            this.dgGrp.RowHeadersVisible = false;
            this.dgGrp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgGrp.Size = new System.Drawing.Size(120, 317);
            this.dgGrp.TabIndex = 11;
            this.dgGrp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgGrp_CellClick);
            this.dgGrp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgGrp_MouseDown);
            // 
            // кодГрDataGridViewTextBoxColumn
            // 
            this.кодГрDataGridViewTextBoxColumn.DataPropertyName = "КодГр";
            this.кодГрDataGridViewTextBoxColumn.HeaderText = "КодГр";
            this.кодГрDataGridViewTextBoxColumn.Name = "кодГрDataGridViewTextBoxColumn";
            this.кодГрDataGridViewTextBoxColumn.Visible = false;
            // 
            // группаDataGridViewTextBoxColumn
            // 
            this.группаDataGridViewTextBoxColumn.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn.Name = "группаDataGridViewTextBoxColumn";
            // 
            // учОтделDataGridViewTextBoxColumn
            // 
            this.учОтделDataGridViewTextBoxColumn.DataPropertyName = "Уч_Отдел";
            this.учОтделDataGridViewTextBoxColumn.HeaderText = "Уч_Отдел";
            this.учОтделDataGridViewTextBoxColumn.Name = "учОтделDataGridViewTextBoxColumn";
            this.учОтделDataGridViewTextBoxColumn.Visible = false;
            // 
            // dgStd
            // 
            this.dgStd.AllowUserToAddRows = false;
            this.dgStd.AllowUserToResizeColumns = false;
            this.dgStd.AllowUserToResizeRows = false;
            this.dgStd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgStd.AutoGenerateColumns = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgStd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодСтDataGridViewTextBoxColumn,
            this.фамилияDataGridViewTextBoxColumn,
            this.фамилияДDataGridViewTextBoxColumn,
            this.иОDataGridViewTextBoxColumn,
            this.формаОбучDataGridViewTextBoxColumn,
            this.группаDataGridViewTextBoxColumn1});
            this.dgStd.DataSource = this.студентыBindingSource;
            this.dgStd.Location = new System.Drawing.Point(141, 25);
            this.dgStd.Name = "dgStd";
            this.dgStd.RowHeadersVisible = false;
            this.dgStd.Size = new System.Drawing.Size(470, 317);
            this.dgStd.TabIndex = 2;
            // 
            // кодСтDataGridViewTextBoxColumn
            // 
            this.кодСтDataGridViewTextBoxColumn.DataPropertyName = "КодСт";
            this.кодСтDataGridViewTextBoxColumn.HeaderText = "КодСт";
            this.кодСтDataGridViewTextBoxColumn.Name = "кодСтDataGridViewTextBoxColumn";
            this.кодСтDataGridViewTextBoxColumn.Visible = false;
            // 
            // фамилияDataGridViewTextBoxColumn
            // 
            this.фамилияDataGridViewTextBoxColumn.DataPropertyName = "Фамилия";
            this.фамилияDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.фамилияDataGridViewTextBoxColumn.Name = "фамилияDataGridViewTextBoxColumn";
            this.фамилияDataGridViewTextBoxColumn.Width = 125;
            // 
            // фамилияДDataGridViewTextBoxColumn
            // 
            this.фамилияДDataGridViewTextBoxColumn.DataPropertyName = "ФамилияД";
            this.фамилияДDataGridViewTextBoxColumn.HeaderText = "ФамилияД";
            this.фамилияДDataGridViewTextBoxColumn.Name = "фамилияДDataGridViewTextBoxColumn";
            this.фамилияДDataGridViewTextBoxColumn.Width = 125;
            // 
            // иОDataGridViewTextBoxColumn
            // 
            this.иОDataGridViewTextBoxColumn.DataPropertyName = "ИО";
            this.иОDataGridViewTextBoxColumn.HeaderText = "ИО";
            this.иОDataGridViewTextBoxColumn.Name = "иОDataGridViewTextBoxColumn";
            // 
            // формаОбучDataGridViewTextBoxColumn
            // 
            this.формаОбучDataGridViewTextBoxColumn.DataPropertyName = "ФормаОбуч";
            this.формаОбучDataGridViewTextBoxColumn.HeaderText = "ФормаОбуч";
            this.формаОбучDataGridViewTextBoxColumn.Name = "формаОбучDataGridViewTextBoxColumn";
            // 
            // группаDataGridViewTextBoxColumn1
            // 
            this.группаDataGridViewTextBoxColumn1.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn1.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn1.Name = "группаDataGridViewTextBoxColumn1";
            this.группаDataGridViewTextBoxColumn1.Visible = false;
            // 
            // Kurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 449);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbAllPredms);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.aqCh);
            this.Controls.Add(this.delPre);
            this.Controls.Add(this.dgPr);
            this.Controls.Add(this.dgShoPr);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.delSt);
            this.Controls.Add(this.adSt);
            this.Controls.Add(this.dgGrp);
            this.Controls.Add(this.bRedGr);
            this.Controls.Add(this.bDelGr);
            this.Controls.Add(this.bAddGr);
            this.Controls.Add(this.lblStds);
            this.Controls.Add(this.dgStd);
            this.Controls.Add(this.lblGrps);
            this.Name = "Kurs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Группы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Kurs_FormClosing);
            this.Load += new System.EventHandler(this.Kurs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.группыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.студентыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.предмГрСеместрBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgShoPr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgGrp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGrps;
        private System.Windows.Forms.Button bAddGr;
        private NDGW dgStd;
        private System.Windows.Forms.Label lblStds;
        private _1DataSet _1DataSet;
        private System.Windows.Forms.BindingSource группыBindingSource;
        private _1DataSetTableAdapters.ГруппыTableAdapter группыTableAdapter;
        private System.Windows.Forms.Button bDelGr;
        private System.Windows.Forms.Button bRedGr;
        private NDGW dgGrp;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодГрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn учОтделDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource студентыBindingSource;
        private System.Windows.Forms.Button delSt;
        private System.Windows.Forms.Button adSt;
        private System.Windows.Forms.Button button1;
        public _1DataSetTableAdapters.СтудентыTableAdapter студентыTableAdapter;
        private NDGW dgShoPr;
        private System.Windows.Forms.BindingSource предмГрСеместрBindingSource;
        private _1DataSetTableAdapters.Предм_Гр_СеместрTableAdapter предм_Гр_СеместрTableAdapter;
        //private System.Windows.Forms.DataGridViewComboBoxColumn cbPredm;
        private System.Windows.Forms.BindingSource предметыBindingSource;
        private _1DataSetTableAdapters.ПредметыTableAdapter предметыTableAdapter;
        private NDGW dgPr;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодПрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn предметDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типПрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn семестрDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button delPre;
        private System.Windows.Forms.Button aqCh;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодСтDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn фамилияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn фамилияДDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn иОDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn формаОбучDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbPredm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbRef;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbUpr;
        private System.Windows.Forms.DataGridViewCheckBoxColumn bLb;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbRZ;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbKR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbKP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbSem;
        private System.Windows.Forms.CheckBox cbAllPredms;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}