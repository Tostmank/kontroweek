﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Threading;

namespace KontroWeek
{
    public partial class Kurs : Form
    {
        bool HaveChanged = false;
        static int  Num;
        int prevsel = 0;
        int prevst = 1, prevstcol = 1;
        static int kG;//текущий год - курс
        public Kurs(int God)
        {
            InitializeComponent();
            Num = God;
            if (DateTime.Now.Month >= 9)
                kG = DateTime.Now.Year-2000 - God + 1;
            else
                kG = DateTime.Now.Year - 2000 - God;
            this.Text = "Группы - Курс " + God.ToString();
            this.группыBindingSource.Filter = "Группа LIKE '%-" + kG.ToString() + "'";
            //this.студентыBindingSource.Filter = "Группа LIKE '%-" + kG.ToString() + "'";
            //this.dgGrp.filter
        }

        void filpr(int grid)
        {
            HaveChanged = false;
            string Filter = "";
            if (cbAllPredms.Checked)
                Filter = "(Группа = " + grid.ToString() + ")";
            else
                Filter = "((Группа = " + grid.ToString() + ") AND ((Семестр = " + (Num * 2).ToString() + ") OR (Семестр = " + (2 * Num - 1).ToString() + ")))";
            this.предмГрСеместрBindingSource.Filter = Filter;
            this.предм_Гр_СеместрTableAdapter.Fill(this._1DataSet.Предм_Гр_Семестр);
             dgShoPr.RowCount = 1;
            if (dgPr.RowCount > 0)
            {
                dgShoPr.RowCount += 1;
                dgShoPr.Rows[0].Cells[0].Value = dgPr.Rows[0].Cells[1].Value;
                dgShoPr.Rows[0].Cells[7].Value = dgPr.Rows[0].Cells[3].Value;
                switch (Convert.ToInt32(dgPr.Rows[0].Cells[2].Value))
                {
                    case 1:
                        dgShoPr.Rows[0].Cells[1].Value = "True";
                        break;
                    case 2:
                        dgShoPr.Rows[0].Cells[2].Value = "True";
                        break;
                    case 3:
                        dgShoPr.Rows[0].Cells[3].Value = "True";
                        break;
                    case 4:
                        dgShoPr.Rows[0].Cells[4].Value = "True";
                        break;
                    case 5:
                        dgShoPr.Rows[0].Cells[5].Value = "True";
                        break;
                    case 6:
                        dgShoPr.Rows[0].Cells[6].Value = "True";
                        break;
                }
                int tel = 2;
                for (int i = 1; i < dgPr.RowCount; i++)
                {
                    if (Convert.ToInt32(dgPr.Rows[i - 1].Cells[1].Value) != Convert.ToInt32(dgPr.Rows[i].Cells[1].Value))
                    {
                        dgShoPr.Rows.Add();
                        dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[0].Value = dgPr.Rows[i].Cells[1].Value;
                        dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[7].Value = dgPr.Rows[i].Cells[3].Value;
                        for (int k = 1; k < 7; k++)
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[k].Value = "Nope";
                    }
                    switch (Convert.ToInt32(dgPr.Rows[i].Cells[2].Value))
                    {
                        case 1:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[1].Value = "True";
                            break;
                        case 2:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[2].Value = "True";
                            break;
                        case 3:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[3].Value = "True";
                            break;
                        case 4:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[4].Value = "True";
                            break;
                        case 5:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[5].Value = "True";
                            break;
                        case 6:
                            dgShoPr.Rows[dgShoPr.RowCount - tel].Cells[6].Value = "True";
                            break;
                    }
                }
            }
        }

        public void fillSt()
        {
            if (dgGrp.RowCount > 0)
            {
                DataRowView dd = ((DataRowView)dgGrp.Rows[dgGrp.SelectedCells[0].RowIndex].DataBoundItem);
                int grid = (int)dd[0];
                this.студентыBindingSource.Filter = "(Группа = " + grid.ToString() + ")";
                this.студентыTableAdapter.Fill(this._1DataSet.Студенты);
                filpr(grid);
            }
            //dgShoPr.RowCount -= 1;
        }
             

        private void Kurs_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet.Предметы". При необходимости она может быть перемещена или удалена.
            this.предметыTableAdapter.Fill(this._1DataSet.Предметы);         
            fill();
            fillSt();
        }

        private void Kurs_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.группыTableAdapter.Update(this._1DataSet);
            this.Hide();
            this.Dispose();
            Statics.MainFrm.Show();
            Statics.MainFrm.Activate();
        }

        public void bAddGr_Click(object sender, EventArgs e)
        {
            if (dgGrp.RowCount > 0)
                prevsel = dgGrp.CurrentCell.RowIndex;
            adGR ag = new adGR(this.группыTableAdapter, this);
            ag.Show();
            ag.Activate();
        }

        public void fill()
        {
            this.группыTableAdapter.Fill(this._1DataSet.Группы);
            if (dgGrp.RowCount > 0)
            {
                if (prevsel >= dgGrp.Rows.Count)
                    prevsel = dgGrp.Rows.Count - 1;
                dgGrp.CurrentCell = dgGrp.Rows[prevsel].Cells[1];
            }
            //dgGrp.CurrentRow = dgGrp.Rows[prevsel];
        }

        private void bRemGr_Click(object sender, EventArgs e)
        {
            try
            {
                prevsel = dgGrp.CurrentCell.RowIndex;
                DataRowView dd = ((DataRowView)dgGrp.SelectedRows[0].DataBoundItem);//  Rows  [0];// lbGroups.SelectedValue;
                this.группыTableAdapter.Delete((int)dd[0], (string)dd[1], (int)dd[2]);//удалить
                this.группыTableAdapter.Update(this._1DataSet);
                fill();
            }
            catch (Exception ex)
            {

            }
        }

        private void bRedGr_Click(object sender, EventArgs e)
        {
            DataRowView dd = ((DataRowView)dgGrp.SelectedRows[0].DataBoundItem);//  Rows  [0];// lbGroups.SelectedValue;
            prevsel = dgGrp.CurrentCell.RowIndex;
            //DataRowView dd = (DataRowView)lbGroups.SelectedValue;
            delGr dg = new delGr((int)dd[0], (string)dd[1], (int)dd[2], this);
            dg.Show();
            dg.Activate();
        }

        public void upd(int id, string gr, int uo, string gr0, int uo0)
        {
            this.группыTableAdapter.Update(gr, uo, id, gr0, uo0);//редактировать
            this.группыTableAdapter.Update(this._1DataSet);
            fill();                       
        }

        private void dgGrp_CellClick(object sender, DataGridViewCellEventArgs e)
        {           
            DataRowView dd = ((DataRowView)dgGrp.SelectedRows[0].DataBoundItem);//  Rows  [0];// lbGroups.SelectedValue;
            fillSt();
        }

        private void fiskiToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.студентыTableAdapter.fiski(this._1DataSet.Студенты);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void adSt_Click(object sender, EventArgs e)
        {
            DataRowView dd = ((DataRowView)dgGrp.SelectedRows[0].DataBoundItem);
            addStu ast = new addStu(dd, this);
            ast.Show();
            ast.Activate();
        }
        
        private void delSt_Click(object sender, EventArgs e)
        {
            try
            {
                prevst = dgStd.CurrentCell.RowIndex;
                prevstcol = dgStd.CurrentCell.ColumnIndex;
                DataRowView dd = ((DataRowView)dgStd.Rows[dgStd.SelectedCells[0].RowIndex].DataBoundItem);//  Rows  [0];// lbGroups.SelectedValue;
                this.студентыTableAdapter.Delete(Convert.ToInt32(dd[0]), (string)dd[1], (string)dd[2], (string)dd[3], (string)dd[4], Convert.ToInt32(dd[5]));//удалить
                this.студентыTableAdapter.Update(this._1DataSet);
                fillSt();
            }
            catch (Exception ex)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            prevst = dgStd.CurrentCell.RowIndex;
            prevstcol = dgStd.CurrentCell.ColumnIndex;
            DataRowView dd = ((DataRowView)dgStd.Rows[dgStd.SelectedCells[0].RowIndex].DataBoundItem);//  Rows  [0];// lbGroups.SelectedValue;
            chG cheng = new chG(kG, dd, this);
            cheng.Show();
            cheng.Activate();            
        }

        public void updSt(DataRowView ded, int NG)
        {
            this.студентыTableAdapter.Update((string)ded[1], (string)ded[2], (string)ded[3], (string)ded[4], NG,
                (int)ded[0], (string)ded[1], (string)ded[2], (string)ded[3], (string)ded[4], (int)ded[5]);
            this.группыTableAdapter.Update(this._1DataSet);
            fillSt();
        }

        private void ndgw1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void delPre_Click(object sender, EventArgs e)
        {
            try
            {
                dgShoPr.Rows.RemoveAt(dgShoPr.CurrentCell.RowIndex);
            }
            catch (Exception ex)
            { }
        }

        private void aqCh_Click(object sender, EventArgs e)
        {
            Remover();
            Add();
            this.предм_Гр_СеместрTableAdapter.Update(this._1DataSet);
            fillSt();
        }

        void Remover()
        {
            int i = 0;
            while (i < dgPr.RowCount)
            {
                DataRowView dd = ((DataRowView)dgPr.Rows[0].DataBoundItem);
                if (Convert.ToInt32(dd[3]) != 0)
                {
                    this.предм_Гр_СеместрTableAdapter.Delete(Convert.ToInt32(dd[0]), Convert.ToInt32(dd[1]), Convert.ToInt32(dd[2]), Convert.ToInt32(dd[3]), Convert.ToInt32(dd[4]));
                    dgPr.Rows.RemoveAt(0);
                }
                else
                {
                    this.предм_Гр_СеместрTableAdapter.Delete(Convert.ToInt32(dd[0]), Convert.ToInt32(dd[1]), Convert.ToInt32(dd[2]), Num*2-1, Convert.ToInt32(dd[4]));
                    dgPr.Rows.RemoveAt(0);
                }
            }
            this.предм_Гр_СеместрTableAdapter.Fill(this._1DataSet.Предм_Гр_Семестр);
        }


        void Add()
        {
            DataRowView dd = ((DataRowView)dgGrp.SelectedRows[0].DataBoundItem);
            int Grp = (int)dd[0];
            int j = 0;
            while (j < dgShoPr.RowCount)
            {
                int arb = 1;
                while (arb < 7)
                {
                    DataGridViewCheckBoxCell dl = dgShoPr.Rows[j].Cells[arb] as DataGridViewCheckBoxCell;
                    if (dl.Value == dl.TrueValue)
                    {

                        this.предм_Гр_СеместрTableAdapter.Insert(Convert.ToInt32(dgShoPr.Rows[j].Cells[0].Value),
                            arb, 
                            Convert.ToInt32(dgShoPr.Rows[j].Cells[7].Value),Grp);
                    };
                    arb++;
                }
                j++;
            }
        }

        private void dgShoPr_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgShoPr.Rows[dgShoPr.RowCount - 2].Cells[7].Value = "1";
            for (int i=1;i<7;i++)
                dgShoPr.Rows[dgShoPr.RowCount - 2].Cells[i].Value = "Nope";
        }

        private void dgShoPr_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Templates Temp = new Templates(this,true);
            Temp.Show();
            Temp.Activate();
        }

        public void AgreeSha(DataGridView DGS)
        {
            while(dgShoPr.RowCount>1)
                dgShoPr.Rows.RemoveAt(0);
            for (int i = 0; i < DGS.RowCount-1; i++)
            {
                DataGridViewRow dd = DGS.Rows[i];
                dgShoPr.Rows.Add();
                for (int j = 0; j < dgShoPr.ColumnCount;j++ )
                    dgShoPr.Rows[i].Cells[j].Value = dd.Cells[j].Value;
            }
        }

        private void dgPr_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgShoPr_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            HaveChanged = true;
        }

        private void dgGrp_MouseDown(object sender, MouseEventArgs e)
        {
            if (HaveChanged)
            {
                if (MessageBox.Show("Сохранить изменения в расписании группы?", "Не сохранили изменения", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Remover();
                    Add();
                    this.предм_Гр_СеместрTableAdapter.Update(this._1DataSet);
                }
                HaveChanged = false;
            }
        }
    }
}
