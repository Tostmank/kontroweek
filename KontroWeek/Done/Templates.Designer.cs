﻿namespace KontroWeek
{
    partial class Templates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this._1DataSet = new KontroWeek._1DataSet();
            this.dgTGrp = new KontroWeek.NDGW();
            this.названиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.шаблоныНазвBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.шаблошкиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bRemSh = new System.Windows.Forms.Button();
            this.bAddSh = new System.Windows.Forms.Button();
            this.dgShoTemp = new KontroWeek.NDGW();
            this.cbPredm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.предметыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbRef = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbUpr = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bLb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbRZ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbKR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbKP = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbSem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bChoose = new System.Windows.Forms.Button();
            this.шаблошкиTableAdapter = new KontroWeek._1DataSetTableAdapters.ШаблошкиTableAdapter();
            this.предметыTableAdapter = new KontroWeek._1DataSetTableAdapters.ПредметыTableAdapter();
            this.шаблоны_НазвTableAdapter = new KontroWeek._1DataSetTableAdapters.Шаблоны_НазвTableAdapter();
            this.bSaveSh = new System.Windows.Forms.Button();
            this.dgSh = new KontroWeek.NDGW();
            this.кодПрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.предметDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типПрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.семестрDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTGrp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.шаблоныНазвBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.шаблошкиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgShoTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSh)).BeginInit();
            this.SuspendLayout();
            // 
            // _1DataSet
            // 
            this._1DataSet.DataSetName = "_1DataSet";
            this._1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dgTGrp
            // 
            this.dgTGrp.AllowUserToAddRows = false;
            this.dgTGrp.AllowUserToResizeColumns = false;
            this.dgTGrp.AllowUserToResizeRows = false;
            this.dgTGrp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgTGrp.AutoGenerateColumns = false;
            this.dgTGrp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTGrp.ColumnHeadersVisible = false;
            this.dgTGrp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.названиеDataGridViewTextBoxColumn});
            this.dgTGrp.DataSource = this.шаблоныНазвBindingSource;
            this.dgTGrp.Location = new System.Drawing.Point(12, 12);
            this.dgTGrp.MultiSelect = false;
            this.dgTGrp.Name = "dgTGrp";
            this.dgTGrp.RowHeadersVisible = false;
            this.dgTGrp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTGrp.Size = new System.Drawing.Size(120, 319);
            this.dgTGrp.TabIndex = 24;
            this.dgTGrp.SelectionChanged += new System.EventHandler(this.dgGrp_SelectionChanged);
            // 
            // названиеDataGridViewTextBoxColumn
            // 
            this.названиеDataGridViewTextBoxColumn.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn.Name = "названиеDataGridViewTextBoxColumn";
            // 
            // шаблоныНазвBindingSource
            // 
            this.шаблоныНазвBindingSource.DataMember = "Шаблоны_Назв";
            this.шаблоныНазвBindingSource.DataSource = this._1DataSet;
            // 
            // шаблошкиBindingSource
            // 
            this.шаблошкиBindingSource.DataMember = "Шаблошки";
            this.шаблошкиBindingSource.DataSource = this._1DataSet;
            // 
            // bRemSh
            // 
            this.bRemSh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bRemSh.Location = new System.Drawing.Point(12, 396);
            this.bRemSh.Name = "bRemSh";
            this.bRemSh.Size = new System.Drawing.Size(120, 23);
            this.bRemSh.TabIndex = 27;
            this.bRemSh.Text = "Очистить шаблон";
            this.bRemSh.UseVisualStyleBackColor = true;
            this.bRemSh.Click += new System.EventHandler(this.bRemSh_Click);
            // 
            // bAddSh
            // 
            this.bAddSh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAddSh.Location = new System.Drawing.Point(13, 337);
            this.bAddSh.Name = "bAddSh";
            this.bAddSh.Size = new System.Drawing.Size(120, 23);
            this.bAddSh.TabIndex = 26;
            this.bAddSh.Text = "Добавить шаблон";
            this.bAddSh.UseVisualStyleBackColor = true;
            this.bAddSh.Click += new System.EventHandler(this.bAddSh_Click);
            // 
            // dgShoTemp
            // 
            this.dgShoTemp.AllowUserToResizeRows = false;
            this.dgShoTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgShoTemp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgShoTemp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgShoTemp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbPredm,
            this.cbRef,
            this.cbUpr,
            this.bLb,
            this.cbRZ,
            this.cbKR,
            this.cbKP,
            this.cbSem});
            this.dgShoTemp.Location = new System.Drawing.Point(139, 12);
            this.dgShoTemp.Name = "dgShoTemp";
            this.dgShoTemp.RowHeadersVisible = false;
            this.dgShoTemp.Size = new System.Drawing.Size(829, 407);
            this.dgShoTemp.TabIndex = 25;
            this.dgShoTemp.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgShoTemp_RowsAdded);
            // 
            // cbPredm
            // 
            this.cbPredm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cbPredm.DataSource = this.предметыBindingSource;
            this.cbPredm.DisplayMember = "Предмет";
            this.cbPredm.HeaderText = "Предмет";
            this.cbPredm.Name = "cbPredm";
            this.cbPredm.ValueMember = "КодПр";
            // 
            // предметыBindingSource
            // 
            this.предметыBindingSource.DataMember = "Предметы";
            this.предметыBindingSource.DataSource = this._1DataSet;
            this.предметыBindingSource.Sort = "Предмет";
            // 
            // cbRef
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.NullValue = false;
            this.cbRef.DefaultCellStyle = dataGridViewCellStyle2;
            this.cbRef.FalseValue = "Nope";
            this.cbRef.HeaderText = "РЕФ";
            this.cbRef.IndeterminateValue = "Nope";
            this.cbRef.Name = "cbRef";
            this.cbRef.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbRef.TrueValue = "True";
            this.cbRef.Width = 40;
            // 
            // cbUpr
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.NullValue = false;
            this.cbUpr.DefaultCellStyle = dataGridViewCellStyle3;
            this.cbUpr.FalseValue = "Nope";
            this.cbUpr.HeaderText = "УПР";
            this.cbUpr.IndeterminateValue = "Nope";
            this.cbUpr.Name = "cbUpr";
            this.cbUpr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbUpr.TrueValue = "True";
            this.cbUpr.Width = 40;
            // 
            // bLb
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.NullValue = false;
            this.bLb.DefaultCellStyle = dataGridViewCellStyle4;
            this.bLb.FalseValue = "Nope";
            this.bLb.HeaderText = "ЛБ";
            this.bLb.IndeterminateValue = "Nope";
            this.bLb.Name = "bLb";
            this.bLb.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bLb.TrueValue = "True";
            this.bLb.Width = 40;
            // 
            // cbRZ
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.NullValue = false;
            this.cbRZ.DefaultCellStyle = dataGridViewCellStyle5;
            this.cbRZ.FalseValue = "Nope";
            this.cbRZ.HeaderText = "РЗ";
            this.cbRZ.IndeterminateValue = "Nope";
            this.cbRZ.Name = "cbRZ";
            this.cbRZ.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbRZ.TrueValue = "True";
            this.cbRZ.Width = 40;
            // 
            // cbKR
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.NullValue = false;
            this.cbKR.DefaultCellStyle = dataGridViewCellStyle6;
            this.cbKR.FalseValue = "Nope";
            this.cbKR.HeaderText = "КР";
            this.cbKR.IndeterminateValue = "Nope";
            this.cbKR.Name = "cbKR";
            this.cbKR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbKR.TrueValue = "True";
            this.cbKR.Width = 40;
            // 
            // cbKP
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.NullValue = false;
            this.cbKP.DefaultCellStyle = dataGridViewCellStyle7;
            this.cbKP.FalseValue = "Nope";
            this.cbKP.HeaderText = "КП";
            this.cbKP.IndeterminateValue = "Nope";
            this.cbKP.Name = "cbKP";
            this.cbKP.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cbKP.TrueValue = "True";
            this.cbKP.Width = 40;
            // 
            // cbSem
            // 
            this.cbSem.HeaderText = "Семестр";
            this.cbSem.Name = "cbSem";
            this.cbSem.Width = 70;
            // 
            // bChoose
            // 
            this.bChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bChoose.Location = new System.Drawing.Point(12, 425);
            this.bChoose.Name = "bChoose";
            this.bChoose.Size = new System.Drawing.Size(120, 23);
            this.bChoose.TabIndex = 28;
            this.bChoose.Text = "Выбрать шаблон";
            this.bChoose.UseVisualStyleBackColor = true;
            this.bChoose.Click += new System.EventHandler(this.bChoose_Click);
            // 
            // шаблошкиTableAdapter
            // 
            this.шаблошкиTableAdapter.ClearBeforeFill = true;
            // 
            // предметыTableAdapter
            // 
            this.предметыTableAdapter.ClearBeforeFill = true;
            // 
            // шаблоны_НазвTableAdapter
            // 
            this.шаблоны_НазвTableAdapter.ClearBeforeFill = true;
            // 
            // bSaveSh
            // 
            this.bSaveSh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bSaveSh.Location = new System.Drawing.Point(13, 366);
            this.bSaveSh.Name = "bSaveSh";
            this.bSaveSh.Size = new System.Drawing.Size(120, 23);
            this.bSaveSh.TabIndex = 30;
            this.bSaveSh.Text = "Сохранить шаблон";
            this.bSaveSh.UseVisualStyleBackColor = true;
            this.bSaveSh.Click += new System.EventHandler(this.bSaveSh_Click);
            // 
            // dgSh
            // 
            this.dgSh.AllowUserToAddRows = false;
            this.dgSh.AllowUserToDeleteRows = false;
            this.dgSh.AutoGenerateColumns = false;
            this.dgSh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодПрDataGridViewTextBoxColumn,
            this.предметDataGridViewTextBoxColumn,
            this.типПрDataGridViewTextBoxColumn,
            this.семестрDataGridViewTextBoxColumn,
            this.названиеDataGridViewTextBoxColumn1});
            this.dgSh.DataSource = this.шаблошкиBindingSource;
            this.dgSh.Location = new System.Drawing.Point(139, 12);
            this.dgSh.Name = "dgSh";
            this.dgSh.Size = new System.Drawing.Size(42, 18);
            this.dgSh.TabIndex = 31;
            this.dgSh.Visible = false;
            // 
            // кодПрDataGridViewTextBoxColumn
            // 
            this.кодПрDataGridViewTextBoxColumn.DataPropertyName = "КодПр";
            this.кодПрDataGridViewTextBoxColumn.HeaderText = "КодПр";
            this.кодПрDataGridViewTextBoxColumn.Name = "кодПрDataGridViewTextBoxColumn";
            // 
            // предметDataGridViewTextBoxColumn
            // 
            this.предметDataGridViewTextBoxColumn.DataPropertyName = "Предмет";
            this.предметDataGridViewTextBoxColumn.HeaderText = "Предмет";
            this.предметDataGridViewTextBoxColumn.Name = "предметDataGridViewTextBoxColumn";
            // 
            // типПрDataGridViewTextBoxColumn
            // 
            this.типПрDataGridViewTextBoxColumn.DataPropertyName = "ТипПр";
            this.типПрDataGridViewTextBoxColumn.HeaderText = "ТипПр";
            this.типПрDataGridViewTextBoxColumn.Name = "типПрDataGridViewTextBoxColumn";
            // 
            // семестрDataGridViewTextBoxColumn
            // 
            this.семестрDataGridViewTextBoxColumn.DataPropertyName = "Семестр";
            this.семестрDataGridViewTextBoxColumn.HeaderText = "Семестр";
            this.семестрDataGridViewTextBoxColumn.Name = "семестрDataGridViewTextBoxColumn";
            // 
            // названиеDataGridViewTextBoxColumn1
            // 
            this.названиеDataGridViewTextBoxColumn1.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn1.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn1.Name = "названиеDataGridViewTextBoxColumn1";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(139, 427);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "Удалить предмет";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Templates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 462);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgSh);
            this.Controls.Add(this.bSaveSh);
            this.Controls.Add(this.bChoose);
            this.Controls.Add(this.dgTGrp);
            this.Controls.Add(this.bRemSh);
            this.Controls.Add(this.bAddSh);
            this.Controls.Add(this.dgShoTemp);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Templates";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Шаблоны";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Templates_FormClosing);
            this.Load += new System.EventHandler(this.Templates_Load);
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTGrp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.шаблоныНазвBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.шаблошкиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgShoTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private _1DataSet _1DataSet;
        private NDGW dgTGrp;
        private System.Windows.Forms.Button bRemSh;
        private System.Windows.Forms.Button bAddSh;
        private NDGW dgShoTemp;
        private System.Windows.Forms.Button bChoose;
        private System.Windows.Forms.BindingSource шаблошкиBindingSource;
        private _1DataSetTableAdapters.ШаблошкиTableAdapter шаблошкиTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource предметыBindingSource;
        private _1DataSetTableAdapters.ПредметыTableAdapter предметыTableAdapter;
        private System.Windows.Forms.BindingSource шаблоныНазвBindingSource;
        private _1DataSetTableAdapters.Шаблоны_НазвTableAdapter шаблоны_НазвTableAdapter;
        private System.Windows.Forms.Button bSaveSh;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbPredm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbRef;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbUpr;
        private System.Windows.Forms.DataGridViewCheckBoxColumn bLb;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbRZ;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbKR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbKP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbSem;
        private NDGW dgSh;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодПрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn предметDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типПрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn семестрDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button button1;

    }
}