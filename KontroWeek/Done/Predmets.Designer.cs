﻿namespace KontroWeek
{
    partial class Predmets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.предметыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._1DataSet = new KontroWeek._1DataSet();
            this.предметыTableAdapter = new KontroWeek._1DataSetTableAdapters.ПредметыTableAdapter();
            this.ndgwPredm = new KontroWeek.NDGW();
            this.кодПрDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.предметDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.predmDel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ndgwPredm)).BeginInit();
            this.SuspendLayout();
            // 
            // предметыBindingSource
            // 
            this.предметыBindingSource.DataMember = "Предметы";
            this.предметыBindingSource.DataSource = this._1DataSet;
            // 
            // _1DataSet
            // 
            this._1DataSet.DataSetName = "_1DataSet";
            this._1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // предметыTableAdapter
            // 
            this.предметыTableAdapter.ClearBeforeFill = true;
            // 
            // ndgwPredm
            // 
            this.ndgwPredm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ndgwPredm.AutoGenerateColumns = false;
            this.ndgwPredm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ndgwPredm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодПрDataGridViewTextBoxColumn2,
            this.предметDataGridViewTextBoxColumn2});
            this.ndgwPredm.DataSource = this.предметыBindingSource;
            this.ndgwPredm.Location = new System.Drawing.Point(12, 12);
            this.ndgwPredm.Name = "ndgwPredm";
            this.ndgwPredm.Size = new System.Drawing.Size(960, 409);
            this.ndgwPredm.TabIndex = 6;
            // 
            // кодПрDataGridViewTextBoxColumn2
            // 
            this.кодПрDataGridViewTextBoxColumn2.DataPropertyName = "КодПр";
            this.кодПрDataGridViewTextBoxColumn2.HeaderText = "КодПр";
            this.кодПрDataGridViewTextBoxColumn2.Name = "кодПрDataGridViewTextBoxColumn2";
            // 
            // предметDataGridViewTextBoxColumn2
            // 
            this.предметDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.предметDataGridViewTextBoxColumn2.DataPropertyName = "Предмет";
            this.предметDataGridViewTextBoxColumn2.HeaderText = "Предмет";
            this.предметDataGridViewTextBoxColumn2.Name = "предметDataGridViewTextBoxColumn2";
            // 
            // predmDel
            // 
            this.predmDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.predmDel.Location = new System.Drawing.Point(12, 427);
            this.predmDel.Name = "predmDel";
            this.predmDel.Size = new System.Drawing.Size(168, 23);
            this.predmDel.TabIndex = 5;
            this.predmDel.Text = "Удалить выделенный предмет";
            this.predmDel.UseVisualStyleBackColor = true;
            this.predmDel.Click += new System.EventHandler(this.predmDel_Click_1);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(186, 427);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Шаблоны";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Predmets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 462);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ndgwPredm);
            this.Controls.Add(this.predmDel);
            this.Name = "Predmets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Предметы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Predmets_FormClosing);
            this.Load += new System.EventHandler(this.Predmets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.предметыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ndgwPredm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private _1DataSet _1DataSet;
        private System.Windows.Forms.BindingSource предметыBindingSource;
        private _1DataSetTableAdapters.ПредметыTableAdapter предметыTableAdapter;
        private NDGW ndgwPredm;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодПрDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn предметDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button predmDel;
        private System.Windows.Forms.Button button1;

    }
}