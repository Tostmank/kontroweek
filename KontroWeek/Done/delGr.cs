﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class delGr : Form
    {
        int id, uo;
        string gr; Kurs pa;
        public delGr(int id,string gr, int uo, Kurs pa)
        {
            InitializeComponent();
            this.id = id;
            this.gr = gr;
            this.uo = uo;
            this.pa = pa;
        }

        private void delGr_Load(object sender, EventArgs e)
        {
            this.уч_ОтделыTableAdapter.Fill(this._1DataSet.Уч_Отделы);
            comboBox2.Text = (string)(((DataRowView)comboBox2.Items[uo-1])[1]);
            comboBox2.Select(uo+1,0);
            grName.Text = gr;
        }

        private void delGr_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        { 
            bool cont = true;
            if (grName.Text == "")
            {
                MessageBox.Show("Назовите группу!");
                cont = false;
            }
            if (comboBox2.Text == "")
            {
                MessageBox.Show("Выберите факультет!");
                cont = false;
            }
            if (cont)
            {
                pa.upd(id, grName.Text, comboBox2.SelectedIndex + 1, gr, uo);
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
