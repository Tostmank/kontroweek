﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Threading;
using System.IO;

namespace KontroWeek
{
    public partial class Form1 : Form
    {
        //Kurs Kurs;
        public OleDbConnection DBConn;
        public OleDbCommand oleDbCmd = new OleDbCommand();
        public BindingSource myBS = new BindingSource();
        private const string nameDb = "1.mdb";
        public String connParam = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Directory.GetCurrentDirectory() + @"\" + nameDb;
        //public String connParam = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\Documents and Settings\SqlAdm\Рабочий стол\uchebk\todoprogs\KontroWeek\KontroWeek\1.mdb;Persist Security Info=False";

        public Form1()
        {
            DBConn = new OleDbConnection(connParam);
            Statics.odbCon = DBConn;
            Statics.conn = connParam;
            InitializeComponent();
        }

        private void bPredmets_Click(object sender, EventArgs e)
        {
            Predmets prdm = new Predmets();
            Shorta(prdm);
        }

        private void bKurs1_Click(object sender, EventArgs e)
        {
            Kurs kek = new Kurs(1);
            Shorta(kek);
        }

        private void bKurs2_Click(object sender, EventArgs e)
        {
            Kurs kek = new Kurs(2);
            Shorta(kek);
        }

        private void bKurs3_Click(object sender, EventArgs e)
        {
            Kurs kek = new Kurs(3);
            Shorta(kek);
        }

        private void bKurs4_Click(object sender, EventArgs e)
        {
            Kurs kek = new Kurs(4);
            Shorta(kek);
        }

        private void bImport_Click(object sender, EventArgs e)
        {
            LoadData LD = new LoadData();
            Shorta(LD);
        }

        private void bOptions_Click(object sender, EventArgs e)
        {
            Options Op = new Options();
            Shorta(Op);
        }

        private void bPapers_Click(object sender, EventArgs e)
        {
            Tickets Ot4 = new Tickets();
            Shorta(Ot4);
        }

        void Shorta(object form)
        {
            Form cb = (Form)form;
            cb.Show();
            cb.Activate();
            this.Hide();
            this.SendToBack();
        }
    }
}
