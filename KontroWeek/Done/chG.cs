﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class chG : Form
    {
        int kurs;
        Kurs mn;
        DataRowView ded;
        public chG(int kurs, DataRowView ded, Kurs mn)
        {
            InitializeComponent();
            this.ded = ded;
            this.kurs = kurs;
            this.mn = mn;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool cont = true;
            if (cbNG.Text == "")
            {
                MessageBox.Show("Назовите группу!");
                cont = false;
            }
            if (cont)
            {
                int koi = (int)(((DataRowView)cbNG.SelectedItem)[0]);
                mn.updSt(ded, koi);
                this.Close();
            }
        }

        private void chG_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet.Группы". При необходимости она может быть перемещена или удалена.
            this.группыBindingSource.Filter = "Группа LIKE '%-" + kurs.ToString() + "'";
            this.группыTableAdapter.Fill(this._1DataSet.Группы);
            //tbOldGr.Text = группыBindingSource.Find("Группа", ded[5]);
            int fi = группыBindingSource.Find("КодГр", ded[5]);
            DataRowView Keke = (DataRowView)группыBindingSource[fi];
            tbOldGr.Text = (string)(Keke[1]);
            tbStd.Text = ded[1] + " " + ded[3];
        }
    }
}
