﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace KontroWeek
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        private void Options_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet1.Парам_Взыск". При необходимости она может быть перемещена или удалена.
            this.парам_ВзыскTableAdapter.Fill(this._1DataSet.Парам_Взыск);
            this.парам_ВзыскTableAdapter.Adapter.AcceptChangesDuringUpdate = true;
        }

        private void Options_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool cont = true;
            string predb = "";
            if (PredB.Text == "")
            {
                predb += "Поле 'Предупреждение-Балл' не заполнено \r\n";
                cont = false;
            };
            if (PredP.Text == "")
            {
                predb += "Поле 'Предупреждение-Пропусков' не заполнено \r\n";
                cont = false;
            };
            if (PredP2.Text == "")
            {
                predb += ("Поле 'Предупреждение-Пропусков[2]' не заполнено \r\n");
                cont = false;
            };
            if (ZamB.Text == "")
            {
                predb += ("Поле 'Замечание-Балл' не заполнено \r\n");
                cont = false;
            };
            if (ZamP.Text == "")
            {
                predb += ("Поле 'Замечание-Пропусков' не заполнено \r\n");
                cont = false;
            };
            if (Zamp2.Text == "")
            {
                predb += ("Поле 'Замечание-Пропусков[2]' не заполнено \r\n");
                cont = false;
            };
            if (VigB.Text == "")
            {
                predb += ("Поле 'Выговор-Балл' не заполнено \r\n");
                cont = false;
            };
            if (VigP.Text == "")
            {
                predb += ("Поле 'Выговор-Пропусков' не заполнено \r\n");
                cont = false;
            };
            if (VigP2.Text == "")
            {
                predb += ("Поле 'Выговор-Пропусков[2]' не заполнено \r\n");
                cont = false;
            };
            if (BestB.Text == "")
            {
                predb += ("Поле 'Лучшие-Балл' не заполнено \r\n");
                cont = false;
            };
            if (VigPovt.Text == "")
            {
                predb += ("Поле 'Выговор-Повторный' не заполнено \r\n");
                cont = false;
            };
            if (predb.Length > 0)
                MessageBox.Show(predb);
            if (cont)
            {
                /*this.Validate();
                */this._1DataSet.Парам_Взыск[0].BeginEdit();
                this._1DataSet.Парам_Взыск[0].ПредБ = float.Parse(PredB.Text);
                this._1DataSet.Парам_Взыск[0].ЗамБ = float.Parse(ZamB.Text);
                this._1DataSet.Парам_Взыск[0].Лучш = float.Parse(BestB.Text);
                this._1DataSet.Парам_Взыск[0].ВыгБ = float.Parse(VigB.Text);
                this._1DataSet.Парам_Взыск[0].ПредП = byte.Parse(PredP.Text);
                this._1DataSet.Парам_Взыск[0].ПредП2 = byte.Parse(PredP2.Text);
                this._1DataSet.Парам_Взыск[0].ЗамП = byte.Parse(ZamP.Text);
                this._1DataSet.Парам_Взыск[0].ЗамП2 = byte.Parse(Zamp2.Text);
                this._1DataSet.Парам_Взыск[0].ВыгП = byte.Parse(VigP.Text);
                this._1DataSet.Парам_Взыск[0].ВыгП2 = byte.Parse(VigP2.Text);
                this._1DataSet.Парам_Взыск[0].ВыгПовтор = byte.Parse(VigPovt.Text);
                this._1DataSet.Парам_Взыск[0].EndEdit();
                //*/
                this.парам_ВзыскTableAdapter.Update(this._1DataSet.Парам_Взыск[0]);//.Update(this._1DataSet.Парам_Взыск);*/
                Statics.MainFrm.Show();
                Statics.MainFrm.Activate();
            }
            else
                e.Cancel = true;
        }

        private void predB_Leave(object sender, EventArgs e)
        {
            this.Validate();
        }
    }
}
