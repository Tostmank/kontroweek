﻿namespace KontroWeek
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BestB = new System.Windows.Forms.TextBox();
            this.VigPovt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.VigP2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.VigB = new System.Windows.Forms.TextBox();
            this.VigP = new System.Windows.Forms.TextBox();
            this.Zamp2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ZamB = new System.Windows.Forms.TextBox();
            this.ZamP = new System.Windows.Forms.TextBox();
            this.PredP2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PredB = new System.Windows.Forms.TextBox();
            this.PredP = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.парамВзыскBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._1DataSet = new KontroWeek._1DataSet();
            this.парам_ВзыскTableAdapter = new KontroWeek._1DataSetTableAdapters.Парам_ВзыскTableAdapter();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.парамВзыскBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.BestB);
            this.groupBox1.Controls.Add(this.VigPovt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.VigP2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.VigB);
            this.groupBox1.Controls.Add(this.VigP);
            this.groupBox1.Controls.Add(this.Zamp2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ZamB);
            this.groupBox1.Controls.Add(this.ZamP);
            this.groupBox1.Controls.Add(this.PredP2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.PredB);
            this.groupBox1.Controls.Add(this.PredP);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 182);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Административные взыскания";
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(6, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 15);
            this.label7.TabIndex = 19;
            this.label7.Text = "Лучшие";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BestB
            // 
            this.BestB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "Лучш", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BestB.Location = new System.Drawing.Point(122, 151);
            this.BestB.Name = "BestB";
            this.BestB.Size = new System.Drawing.Size(63, 20);
            this.BestB.TabIndex = 20;
            // 
            // VigPovt
            // 
            this.VigPovt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ВыгПовтор", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.VigPovt.Location = new System.Drawing.Point(266, 125);
            this.VigPovt.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.VigPovt.Name = "VigPovt";
            this.VigPovt.Size = new System.Drawing.Size(59, 20);
            this.VigPovt.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(6, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "Повторный выговор";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VigP2
            // 
            this.VigP2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ВыгП2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.VigP2.Location = new System.Drawing.Point(266, 99);
            this.VigP2.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.VigP2.Name = "VigP2";
            this.VigP2.Size = new System.Drawing.Size(59, 20);
            this.VigP2.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(6, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Выговор";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VigB
            // 
            this.VigB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ВыгБ", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.VigB.Location = new System.Drawing.Point(122, 99);
            this.VigB.Name = "VigB";
            this.VigB.Size = new System.Drawing.Size(63, 20);
            this.VigB.TabIndex = 12;
            // 
            // VigP
            // 
            this.VigP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ВыгП", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.VigP.Location = new System.Drawing.Point(201, 99);
            this.VigP.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.VigP.Name = "VigP";
            this.VigP.Size = new System.Drawing.Size(59, 20);
            this.VigP.TabIndex = 13;
            // 
            // Zamp2
            // 
            this.Zamp2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ЗамП2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Zamp2.Location = new System.Drawing.Point(266, 73);
            this.Zamp2.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.Zamp2.Name = "Zamp2";
            this.Zamp2.Size = new System.Drawing.Size(59, 20);
            this.Zamp2.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(6, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Замечание";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ZamB
            // 
            this.ZamB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ЗамБ", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ZamB.Location = new System.Drawing.Point(122, 73);
            this.ZamB.Name = "ZamB";
            this.ZamB.Size = new System.Drawing.Size(63, 20);
            this.ZamB.TabIndex = 8;
            // 
            // ZamP
            // 
            this.ZamP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ЗамП", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ZamP.Location = new System.Drawing.Point(201, 73);
            this.ZamP.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.ZamP.Name = "ZamP";
            this.ZamP.Size = new System.Drawing.Size(59, 20);
            this.ZamP.TabIndex = 9;
            // 
            // PredP2
            // 
            this.PredP2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ПредП2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PredP2.Location = new System.Drawing.Point(266, 47);
            this.PredP2.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.PredP2.Name = "PredP2";
            this.PredP2.Size = new System.Drawing.Size(59, 20);
            this.PredP2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(201, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Пропусков";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(122, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Балл";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Предупреждение";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PredB
            // 
            this.PredB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ПредБ", true));
            this.PredB.Location = new System.Drawing.Point(122, 47);
            this.PredB.Name = "PredB";
            this.PredB.Size = new System.Drawing.Size(63, 20);
            this.PredB.TabIndex = 2;
            this.PredB.Leave += new System.EventHandler(this.predB_Leave);
            // 
            // PredP
            // 
            this.PredP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.парамВзыскBindingSource, "ПредП", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PredP.Location = new System.Drawing.Point(201, 47);
            this.PredP.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.PredP.Name = "PredP";
            this.PredP.Size = new System.Drawing.Size(59, 20);
            this.PredP.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 200);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(342, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Периоды контрольных недель";
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Location = new System.Drawing.Point(242, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 16);
            this.label10.TabIndex = 28;
            this.label10.Text = "2 КН";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(242, 69);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(83, 20);
            this.textBox15.TabIndex = 27;
            this.textBox15.Text = "Апр-Авг";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(122, 69);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(83, 20);
            this.textBox13.TabIndex = 26;
            this.textBox13.Text = "Янв-Март";
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(242, 45);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(83, 20);
            this.textBox12.TabIndex = 25;
            this.textBox12.Text = "Нояб-Дек";
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(122, 45);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(83, 20);
            this.textBox11.TabIndex = 24;
            this.textBox11.Text = "Сент-Окт";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Location = new System.Drawing.Point(6, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 15);
            this.label11.TabIndex = 23;
            this.label11.Text = "Весенний семестр";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(6, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Осенний семестр";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(122, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "1 КН";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // парамВзыскBindingSource
            // 
            this.парамВзыскBindingSource.DataMember = "Парам_Взыск";
            this.парамВзыскBindingSource.DataSource = this._1DataSet;
            // 
            // _1DataSet
            // 
            this._1DataSet.DataSetName = "_1DataSet";
            this._1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // парам_ВзыскTableAdapter
            // 
            this.парам_ВзыскTableAdapter.ClearBeforeFill = true;
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(368, 312);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Опции";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Options_FormClosing);
            this.Load += new System.EventHandler(this.Options_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.парамВзыскBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._1DataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox BestB;
        private System.Windows.Forms.TextBox VigPovt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox VigP2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox VigB;
        private System.Windows.Forms.TextBox VigP;
        private System.Windows.Forms.TextBox Zamp2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ZamB;
        private System.Windows.Forms.TextBox ZamP;
        private System.Windows.Forms.TextBox PredP2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PredB;
        private System.Windows.Forms.TextBox PredP;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private _1DataSet _1DataSet;
        private System.Windows.Forms.BindingSource парамВзыскBindingSource;
        private _1DataSetTableAdapters.Парам_ВзыскTableAdapter парам_ВзыскTableAdapter;

    }
}