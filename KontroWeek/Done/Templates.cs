﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace KontroWeek
{
    public partial class Templates : Form
    {
        Kurs par;
        bool ku;
        Thread thR, thA;
        public Templates(Kurs k, bool ku)
        {
            InitializeComponent();
            this.ku = ku;
            par = k;
        }

        private void Templates_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet.Шаблоны_Назв". При необходимости она может быть перемещена или удалена.
            this.шаблоны_НазвTableAdapter.Fill(this._1DataSet.Шаблоны_Назв);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet.Шаблошки". При необходимости она может быть перемещена или удалена.
            this.предметыTableAdapter.Fill(this._1DataSet.Предметы);
            bChoose.Visible = ku;
            fillGr();
        }

        public void adSHA(string name)
        {
            шаблоны_НазвTableAdapter.Insert(name);
            this.шаблоны_НазвTableAdapter.Fill(this._1DataSet.Шаблоны_Назв);
        }

        private void bAddSh_Click(object sender, EventArgs e)
        {
            adSh asa = new adSh(this);
            asa.Show();
            asa.Activate();
        }

        void Remover()
        {
            int i = 0;
            while (i < dgSh.RowCount)
            {
                DataRowView dd = ((DataRowView)dgSh.Rows[0].DataBoundItem);
                this.шаблошкиTableAdapter.Delete(Convert.ToInt32(dd[0]), Convert.ToInt32(dd[1]), Convert.ToInt32(dd[2]), Convert.ToInt32(dd[3]), Convert.ToInt32(dd[4]));
                //this.шаблошкиTableAdapter.Update(this._1DataSet);
                dgSh.Rows.RemoveAt(0);
            }
            this.шаблошкиTableAdapter.Fill(this._1DataSet.Шаблошки);
        }

        private void bRemSh_Click(object sender, EventArgs e)
        {
            Remover();
            fillGr();
        }

        void fillGr()
        {
            DataRowView dd = ((DataRowView)dgTGrp.Rows[dgTGrp.SelectedCells[0].RowIndex].DataBoundItem);
            int grid = (int)dd[0];

            int jj = 0;
            while (jj < dgSh.RowCount)
            {
                dgSh.Rows.RemoveAt(0);
            };
            while (jj < dgShoTemp.RowCount - 1)
            {
                dgShoTemp.Rows.RemoveAt(0);
            }

            this.шаблошкиTableAdapter.ClearBeforeFill = true;
            this.шаблошкиBindingSource.Filter = "Название = " + grid.ToString();
            this.шаблошкиTableAdapter.Fill(this._1DataSet.Шаблошки);
            if (dgSh.RowCount > 0)
            {
                dgShoTemp.RowCount = 2;
                dgShoTemp.Rows[0].Cells[0].Value = dgSh.Rows[0].Cells[1].Value;
                dgShoTemp.Rows[0].Cells[7].Value = dgSh.Rows[0].Cells[3].Value;
                for (int k = 1; k < 7; k++)
                    dgShoTemp.Rows[0].Cells[k].Value = "Nope";
                switch (Convert.ToInt32(dgSh.Rows[0].Cells[2].Value))
                {
                    case 1:
                        dgShoTemp.Rows[0].Cells[1].Value = "True";
                        break;
                    case 2:
                        dgShoTemp.Rows[0].Cells[2].Value = "True";
                        break;
                    case 3:
                        dgShoTemp.Rows[0].Cells[3].Value = "True";
                        break;
                    case 4:
                        dgShoTemp.Rows[0].Cells[4].Value = "True";
                        break;
                    case 5:
                        dgShoTemp.Rows[0].Cells[5].Value = "True";
                        break;
                    case 6:
                        dgShoTemp.Rows[0].Cells[6].Value = "True";
                        break;
                }
                int tel = 2;
                for (int i = 1; i < dgSh.RowCount; i++)
                {
                    if (Convert.ToInt32(dgSh.Rows[i - 1].Cells[1].Value) != Convert.ToInt32(dgSh.Rows[i].Cells[1].Value))
                    {
                        dgShoTemp.Rows.Add();
                        dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[0].Value = dgSh.Rows[i].Cells[1].Value;
                        dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[7].Value = dgSh.Rows[i].Cells[3].Value;
                        for (int k = 1; k < 7; k++)
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[k].Value = "Nope";
                    }
                    switch (Convert.ToInt32(dgSh.Rows[i].Cells[2].Value))
                    {
                        case 1:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[1].Value = "True";
                            break;
                        case 2:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[2].Value = "True";
                            break;
                        case 3:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[3].Value = "True";
                            break;
                        case 4:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[4].Value = "True";
                            break;
                        case 5:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[5].Value = "True";
                            break;
                        case 6:
                            dgShoTemp.Rows[dgShoTemp.RowCount - tel].Cells[6].Value = "True";
                            break;
                    }
                }
            }
        }

        private void bChoose_Click(object sender, EventArgs e)
        {
            par.AgreeSha(this.dgShoTemp);
            this.Close();
        }

        private void dgGrp_SelectionChanged(object sender, EventArgs e)
        {
            /*if (dgTGrp.SelectedCells == null)
                dgTGrp.Rows[0].Selected = true;// Select();
            if (dgTGrp.SelectedCells[0].RowIndex<dgTGrp.RowCount)*/
                fillGr();
        }

        private void Templates_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.шаблоны_НазвTableAdapter.Update(this._1DataSet.Шаблоны_Назв);
        }

        void bSaveSh_Click(object sender, EventArgs e)
        {
            bool ret = true;
            for (int i = 0; i < dgShoTemp.RowCount - 1; i++)
                if ((dgShoTemp.Rows[i].Cells[0].Value == "") || (dgShoTemp.Rows[i].Cells[7].Value == ""))
                {
                    int cha = 0;
                    ret = false;
                    if (dgShoTemp.Rows[i].Cells[0].Value == "")
                        cha += 1;
                    if (dgShoTemp.Rows[i].Cells[7].Value == "")
                        cha += 2;
                    switch (cha)
                    {
                        case 1:
                            MessageBox.Show("Не выбраны предметы!");
                            break;
                        case 2:
                            MessageBox.Show("Не выбран семестр!");
                            break;
                        case 3:
                            MessageBox.Show("Не выбран семестр и предметы!");
                            break;
                        case 0:
                        default:
                            break;
                    }
                }
            if (ret)
            {
                Remover();
                Add();
                fillGr();
            }
        }

        void Add()
        {
            DataRowView dd = ((DataRowView)dgTGrp.SelectedRows[0].DataBoundItem);
            int Grp = (int)dd[0];
            int j = 0;
            while (j < dgShoTemp.RowCount)
            {
                int arb = 1;
                while (arb<7)
                {
                    DataGridViewCheckBoxCell dl = dgShoTemp.Rows[j].Cells[arb] as DataGridViewCheckBoxCell;
                    if (dl.Value == dl.TrueValue)
                    {
                        шаблошкиTableAdapter.Insert( Convert.ToInt32(dgShoTemp.Rows[j].Cells[0].Value),
                            arb,Grp,
                            Convert.ToInt32(dgShoTemp.Rows[j].Cells[7].Value)
                            );
                        шаблошкиTableAdapter.Update(this._1DataSet.Шаблошки);
                    };
                    arb++;
                }
                j++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dgShoTemp.Rows.RemoveAt(dgShoTemp.CurrentCell.RowIndex);
            }
            catch (Exception ex)
            { }              
            //this.шаблошкиTableAdapter.Update(this._1DataSet);
        }

        private void dgShoTemp_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgShoTemp.Rows[dgShoTemp.RowCount - 2].Cells[7].Value = "1";
            for (int i = 1; i < 7; i++)
                dgShoTemp.Rows[dgShoTemp.RowCount - 2].Cells[i].Value = "Nope";
        }
    }
}
