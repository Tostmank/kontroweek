﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class adGR : Form
    {
        _1DataSetTableAdapters.ГруппыTableAdapter grTA;
        Kurs pa;
        public adGR(_1DataSetTableAdapters.ГруппыTableAdapter kk, Kurs pa)
        {
            InitializeComponent();
            grTA = kk;
            this.pa = pa;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void adGR_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void adGR_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet.Уч_Отделы". При необходимости она может быть перемещена или удалена.
            this.уч_ОтделыTableAdapter.Fill(this._1DataSet.Уч_Отделы);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool cont = true;
            if (grName.Text == "")
            {
                MessageBox.Show("Назовите группу!");
                cont = false;
            }
            if (comboBox2.Text == "")
            {
                MessageBox.Show("Выберите факультет!");
                cont = false;
            }
            if (cont)
            {
                grTA.Insert(grName.Text, comboBox2.SelectedIndex + 1);
                pa.fill();
                this.Close();
            }
        }
    }
}
