﻿namespace KontroWeek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bKurs1 = new System.Windows.Forms.Button();
            this.bKurs2 = new System.Windows.Forms.Button();
            this.bKurs3 = new System.Windows.Forms.Button();
            this.bKurs4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bImport = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bPapers = new System.Windows.Forms.Button();
            this.bOptions = new System.Windows.Forms.Button();
            this.bPredmets = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // bKurs1
            // 
            this.bKurs1.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bKurs1.Location = new System.Drawing.Point(24, 24);
            this.bKurs1.Margin = new System.Windows.Forms.Padding(15, 15, 3, 15);
            this.bKurs1.Name = "bKurs1";
            this.bKurs1.Size = new System.Drawing.Size(45, 45);
            this.bKurs1.TabIndex = 0;
            this.bKurs1.Text = "1";
            this.bKurs1.UseVisualStyleBackColor = true;
            this.bKurs1.Click += new System.EventHandler(this.bKurs1_Click);
            // 
            // bKurs2
            // 
            this.bKurs2.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bKurs2.Location = new System.Drawing.Point(24, 99);
            this.bKurs2.Margin = new System.Windows.Forms.Padding(15);
            this.bKurs2.Name = "bKurs2";
            this.bKurs2.Size = new System.Drawing.Size(45, 45);
            this.bKurs2.TabIndex = 8;
            this.bKurs2.Text = "2";
            this.bKurs2.UseVisualStyleBackColor = true;
            this.bKurs2.Click += new System.EventHandler(this.bKurs2_Click);
            // 
            // bKurs3
            // 
            this.bKurs3.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bKurs3.Location = new System.Drawing.Point(24, 174);
            this.bKurs3.Margin = new System.Windows.Forms.Padding(15);
            this.bKurs3.Name = "bKurs3";
            this.bKurs3.Size = new System.Drawing.Size(45, 45);
            this.bKurs3.TabIndex = 9;
            this.bKurs3.Text = "3";
            this.bKurs3.UseVisualStyleBackColor = true;
            this.bKurs3.Click += new System.EventHandler(this.bKurs3_Click);
            // 
            // bKurs4
            // 
            this.bKurs4.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bKurs4.Location = new System.Drawing.Point(24, 249);
            this.bKurs4.Margin = new System.Windows.Forms.Padding(15);
            this.bKurs4.Name = "bKurs4";
            this.bKurs4.Size = new System.Drawing.Size(45, 45);
            this.bKurs4.TabIndex = 10;
            this.bKurs4.Text = "4";
            this.bKurs4.UseVisualStyleBackColor = true;
            this.bKurs4.Click += new System.EventHandler(this.bKurs4_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(75, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 23);
            this.label1.TabIndex = 11;
            this.label1.Text = "Первый курс";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(75, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 23);
            this.label2.TabIndex = 12;
            this.label2.Text = "Второй курс";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(75, 187);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 23);
            this.label3.TabIndex = 13;
            this.label3.Text = "Третий курс";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(75, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 23);
            this.label4.TabIndex = 14;
            this.label4.Text = "Четвёртый курс";
            // 
            // bImport
            // 
            this.bImport.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bImport.Image = global::KontroWeek.Properties.Resources.Import;
            this.bImport.Location = new System.Drawing.Point(221, 249);
            this.bImport.Name = "bImport";
            this.bImport.Size = new System.Drawing.Size(45, 45);
            this.bImport.TabIndex = 18;
            this.bImport.UseVisualStyleBackColor = true;
            this.bImport.Click += new System.EventHandler(this.bImport_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(272, 37);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 23);
            this.label5.TabIndex = 19;
            this.label5.Text = "Предметы";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(272, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 23);
            this.label6.TabIndex = 20;
            this.label6.Text = "Опции";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(272, 187);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 23);
            this.label7.TabIndex = 21;
            this.label7.Text = "Отчёты";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(272, 262);
            this.label8.Margin = new System.Windows.Forms.Padding(3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 23);
            this.label8.TabIndex = 22;
            this.label8.Text = "Загрузка данных";
            // 
            // bPapers
            // 
            this.bPapers.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPapers.Image = global::KontroWeek.Properties.Resources.Tickets;
            this.bPapers.Location = new System.Drawing.Point(221, 174);
            this.bPapers.Name = "bPapers";
            this.bPapers.Size = new System.Drawing.Size(45, 45);
            this.bPapers.TabIndex = 17;
            this.bPapers.UseVisualStyleBackColor = true;
            this.bPapers.Click += new System.EventHandler(this.bPapers_Click);
            // 
            // bOptions
            // 
            this.bOptions.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bOptions.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOptions.Image = global::KontroWeek.Properties.Resources.Opts;
            this.bOptions.Location = new System.Drawing.Point(221, 99);
            this.bOptions.Margin = new System.Windows.Forms.Padding(15, 15, 3, 15);
            this.bOptions.Name = "bOptions";
            this.bOptions.Size = new System.Drawing.Size(45, 45);
            this.bOptions.TabIndex = 16;
            this.bOptions.UseVisualStyleBackColor = false;
            this.bOptions.Click += new System.EventHandler(this.bOptions_Click);
            // 
            // bPredmets
            // 
            this.bPredmets.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPredmets.Image = global::KontroWeek.Properties.Resources.Subjs;
            this.bPredmets.Location = new System.Drawing.Point(221, 24);
            this.bPredmets.Name = "bPredmets";
            this.bPredmets.Size = new System.Drawing.Size(45, 45);
            this.bPredmets.TabIndex = 15;
            this.bPredmets.UseVisualStyleBackColor = true;
            this.bPredmets.Click += new System.EventHandler(this.bPredmets_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 316);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bImport);
            this.Controls.Add(this.bPapers);
            this.Controls.Add(this.bOptions);
            this.Controls.Add(this.bPredmets);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bKurs4);
            this.Controls.Add(this.bKurs3);
            this.Controls.Add(this.bKurs2);
            this.Controls.Add(this.bKurs1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ИС учебного отдела";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bKurs1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button bKurs2;
        private System.Windows.Forms.Button bKurs3;
        private System.Windows.Forms.Button bKurs4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bPredmets;
        private System.Windows.Forms.Button bOptions;
        private System.Windows.Forms.Button bPapers;
        private System.Windows.Forms.Button bImport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }

    public class NDGW : System.Windows.Forms.DataGridView
    {
        public NDGW() { DoubleBuffered = true; }
    }

}

