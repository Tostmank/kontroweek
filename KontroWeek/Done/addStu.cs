﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class addStu : Form
    {
        Kurs pa;
        DataRowView dd;
        public addStu(DataRowView dd, Kurs pa)
        {
            InitializeComponent();
            this.dd = dd;
            this.pa = pa;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            string io = maskedTextBox1.Text;
            io = io.Replace(".", "");
            try
            {
                //grTA.Insert(grName.Text, comboBox2.SelectedIndex + 1);
                pa.студентыTableAdapter.Insert(textBox2.Text, textBox3.Text, io, comboBox1.Text, (int)dd[0]);
                pa.fillSt();
                if (cbClAAdd.Checked)
                    this.Close();
                else
                {
                    textBox2.Clear();
                    textBox3.Clear();
                    maskedTextBox1.Clear();
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("Одно или несколько полей заданы неверно");
            }
            //студентыTableAdapter.Insert(textBox2.Text, textBox3.Text, maskedTextBox1.Text, comboBox1.Text, (int)dd[0]);
            //pa.студентыTableAdapter.Insert(textBox2.Text, textBox3.Text, maskedTextBox1.Text, comboBox1.Text, (int)dd[0]);
            //.студентыTableAdapter.Insert((string)"а", (string)"а", (string)"аа", (string)"Б", (int)2);
        }

        private void addStu_Load(object sender, EventArgs e)
        {
            comboBox1.Select(1, 1);// (0);
            this.студентыTableAdapter.Fill(this._1DataSet.Студенты);
            this.Text += " в " + (string)dd[1];
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox3.Text = textBox2.Text;
        }

        private void addStu_FormClosing(object sender, FormClosingEventArgs e)
        {
            //pa.fillSt();
        }
    }
}
