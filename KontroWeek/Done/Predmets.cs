﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class Predmets : Form
    {
        public Predmets()
        {
            InitializeComponent();
        }

        private void Predmets_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_1DataSet1.Предметы". При необходимости она может быть перемещена или удалена.
            this.предметыTableAdapter.Fill(this._1DataSet.Предметы);
        }

        private void Predmets_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.предметыTableAdapter.Update(this._1DataSet.Предметы);
            Statics.MainFrm.Show();
            Statics.MainFrm.Activate();
        }

        private void predmDel_Click(object sender, EventArgs e)
        {
            if (ndgwPredm.CurrentCell.Selected)
                ndgwPredm.Rows.RemoveAt(ndgwPredm.CurrentCell.RowIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Templates Temp = new Templates(null, false);
            Temp.Show();
            Temp.Activate();
        }

        private void predmDel_Click_1(object sender, EventArgs e)
        {

        }

    }
}
