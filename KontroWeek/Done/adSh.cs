﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KontroWeek
{
    public partial class adSh : Form
    {
        Templates te;
        public adSh(Templates Te)
        {
            InitializeComponent();
            te = Te;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (grName.Text.Trim() != "" && grName.Text.Trim() != null && grName.Text.Trim() != " ")
            {
                te.adSHA(grName.Text);
                this.Close();
            }
            else
                MessageBox.Show("Введите название шаблона!");
        }
    }
}
