﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Docs.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace KontroWeek
{
    public partial class LoadData : Form
    {

        // Инфраструктура для доступа к БД
        private OleDbConnection _connection;
        private Excel.Application exApp;
        private const string nameDb = "1.mdb";
        private int month, year;

        public LoadData()
        {
            InitializeComponent();
            _connection = Statics.odbCon;//new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Directory.GetCurrentDirectory() + @"\" + nameDb);
            var strSql = @"SELECT Группы.Группа, Группы.Уч_Отдел FROM Группы INNER JOIN Предм_Гр_Семестр ON Группы.КодГр = Предм_Гр_Семестр.Группа GROUP BY Группы.Группа, Группы.Уч_Отдел, Предм_Гр_Семестр.Семестр, Группы.КодГр HAVING (((Предм_Гр_Семестр.Семестр)=2*(Right(Date(),2)-Right(([Группы].[Группа]),2))+Abs(Month(Date())\9))) ORDER BY Группы.КодГр";
            var groupDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var group = new DataTable();
            groupDt.Fill(group);
            listBox1.DataSource = group;
            listBox1.DisplayMember = "Группа";

            month = DateTime.Today.Month;
            year = DateTime.Today.Year;

            //month = 12;
            //month = 10;
            //year = 2014;
        }

        private void genXl(DataRow groropRow, DataTable rsPVz, int m, int t1)
        {
            var swatch = new Stopwatch();

            string otd = null;
            DataTable rsProp = null, rsAVgBall = null;
            var intRow = 2;
            var intCol2 = 3;
            var nameGrup = groropRow.ItemArray[1].ToString();
            //Факультет группы
            switch (Convert.ToInt32(groropRow.ItemArray[2]))
            {
                case 1:
                    otd = "ФКТЭ";
                    break;
                case 2:
                    otd = "ФЭУ";
                    break;
                case 3:
                    otd = "ФЭЭ";
                    break;
            }
            try
            {
                exApp.ScreenUpdating = false;
                exApp.Workbooks.Add();
                // Получаем набор ссылок на объекты Workbook (на созданные книги)
                var exWorkbooks = exApp.Workbooks;
                // Получаем ссылку на книгу 1 - нумерация от 1
                var exWorkbook = exWorkbooks[1];
                exWorkbook.Windows.Item[1].Zoom = 67;
                exWorkbook.Windows.Item[1].WindowState = Excel.XlWindowState.xlMaximized;
                exWorkbook.Styles.Item["Normal"].Font.Size = 14;

                // ссылка на коллекцию страниц
                var exSheets = exWorkbook.Worksheets;

                // ссылка на страницу
                var exWoSh = (Excel.Worksheet)exSheets.Item[1];

                // имя страницы
                exWoSh.Name = nameGrup;
                //Название группы
                exWoSh.Cells[5, 2].Value = groropRow.ItemArray[0]; //Код группы
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[6, 4]].Merge(Type.Missing);
                exWoSh.Cells[4, 3].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter; //Центровака по горизонтали
                exWoSh.Cells[4, 3].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter; //Центровака по вертикали
                exWoSh.Cells[4, 3].Font.Size = 33; //Установка размера шрифта
                exWoSh.Cells[4, 3].Font.Bold = true; //'Выделить жирным
                exWoSh.Cells[4, 3].Value = nameGrup; //Запись названия группы
                var t = Convert.ToString(groropRow.ItemArray[1]);
                t = t.Remove(0, t.Length - 2);
                exWoSh.Cells[4, 2].Value = (4 * (t1 - Convert.ToInt32(t)) + m); //КН                            
                exWoSh.Columns[3].ColumnWidth = 3; //Столбец для порядкового номера
                exWoSh.Columns[4].ColumnWidth = 21; //Столбец для ФИО
                //Форма обучения
                exWoSh.Range[exWoSh.Cells[4, 5], exWoSh.Cells[6, 5]].Merge(Type.Missing);
                exWoSh.Cells[4, 5].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter; //Центровака по горизонтали
                exWoSh.Cells[4, 5].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter; //Центровака по вертикали
                exWoSh.Cells[4, 5].Font.Bold = true; //Выделить жирным
                exWoSh.Columns[5].ColumnWidth = 1.37; //Ширина столбца
                exWoSh.Cells[4, 5].WrapText = true;
                exWoSh.Cells[4, 5].Value = "Б/П"; //Столбец для формы обучения студентов
                //Лучшие и неуспевающие студенты
                exWoSh.Range[exWoSh.Cells[4, 6], exWoSh.Cells[6, 6]].Merge(); //Объединение ячеек
                exWoSh.Cells[4, 6].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter; //Центровака по горизонтали
                exWoSh.Cells[4, 6].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter; //Центровака по вертикали
                exWoSh.Cells[4, 6].Font.Bold = true; //Выделить жирным
                exWoSh.Columns[6].ColumnWidth = 1.37; //Ширина столбца
                exWoSh.Cells[4, 6].WrapText = true;
                exWoSh.Cells[4, 6].Value = "Л/Н"; //Столбец для указания лучших и неуспевающих студентов
                //Данные для экспорта
                t1 = year - 2000;
                var t2 = (month / 9);
                //Цифра "8" - это число рассматриваемых семестров, то есть 4-ый курс * 2 семестра 
                var strSql = String.Format(
                    "SELECT Студенты.КодСт, Студенты.Фамилия, Студенты.ИО, Студенты.ФормаОбуч FROM Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа" +
                    " WHERE (((Группы.Группа)=\"{0}\") AND ((2*({1}-Right(([Группы].[Группа]),2))+{2})<=8)) ORDER BY Группы.КодГр, Студенты.Фамилия, Студенты.ИО",
                    nameGrup, t1, t2);
                var rsStudDa = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
                var rsStud = new DataTable();
                rsStudDa.Fill(rsStud);

                if (month > 0 && month < 4) t2 = -2;
                else if (month > 3 && month < 9) t2 = -1;
                else if (month > 8 && month < 11) t2 = 0;
                else if (month > 10 && month < 13) t2 = 1;
                //Запрос для экспорта предметов группы
                strSql = @"SELECT * FROM Экспорт_Предметов WHERE (Экспорт_Предметов.КодГр)=" +
                         groropRow.ItemArray[0];
                var rsPredmDa = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
                var rsPredm = new DataTable();
                rsPredmDa.Fill(rsPredm);

                if (m == 0 || m == 2)
                {
                    // Пропуски
                    strSql = String.Format(
                        "SELECT Студенты.КодСт, Успеваемость.Пропуск_уваж, Успеваемость.Пропуск_неуваж " +
                        "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент " +
                        "WHERE (((Группы.Группа)=\"{0}\")) " +
                        "GROUP BY Студенты.КодСт, Успеваемость.Пропуск_уваж, Успеваемость.Пропуск_неуваж, Группы.КодГр, Студенты.Фамилия, Студенты.ИО, Группы.Группа, Успеваемость.[№КН], Студенты.ФормаОбуч " +
                        "HAVING (((Успеваемость.[№КН])=4*({1}-Right(([Группы].[Группа]),2))+{2})) " +
                        "ORDER BY Группы.КодГр, Студенты.Фамилия, Студенты.ИО",
                        nameGrup, t1, t2);
                    var rsPropDa = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
                    rsProp = new DataTable();
                    rsPropDa.Fill(rsProp);

                    // Средний бал
                    strSql = String.Format(
                        "SELECT Студенты.КодСт, Avg(Балл_КН.Балл) AS [Avg-Балл] " +
                        "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт " +
                        "WHERE (((Группы.Группа) = \"{0}\")) " +
                        "GROUP BY Студенты.КодСт, Группы.КодГр, Студенты.Фамилия, Студенты.ИО, Группы.Группа, Балл_КН.№КН, Студенты.ФормаОбуч " +
                        "HAVING (((Балл_КН.№КН) = 4 * ({1} - Right(([Группы].[Группа]), 2)) + {2})) " +
                        "ORDER BY Группы.КодГр, Студенты.Фамилия, Студенты.ИО",
                        nameGrup, t1, t2);
                    var rsAVgBallDa = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
                    rsAVgBall = new DataTable();
                    rsAVgBallDa.Fill(rsAVgBall);
                }

                exWoSh.Rows[4].EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //Выравнивание полей названий предметов
                exWoSh.Rows[4].EntireRow.WrapText = true;
                exWoSh.Rows[4].EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                exWoSh.Rows[5].EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //Центровка в заголовке числа пропусков
                exWoSh.Rows[5].EntireRow.WrapText = true;
                exWoSh.Rows[5].EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                exWoSh.Rows[6].EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //Центровка в поле типа предмета
                exWoSh.Rows[6].EntireRow.WrapText = true;
                exWoSh.Rows[6].EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                exWoSh.Rows[4].RowHeight = 150;

                var k = 0;
                for (var i = 0; i < rsPredm.Rows.Count; i++)
                {
                    var kod = Convert.ToInt32(rsPredm.Rows[i].ItemArray[0]); //Код предмета
                    var kodPredmet = Convert.ToInt32(rsPredm.Rows[i].ItemArray[2]);
                    exWoSh.Cells[2, intCol2 + 4].Value = kod;
                    exWoSh.Cells[5, intCol2 + 4].Value = rsPredm.Rows[i].ItemArray[4]; //Тип предмета
                    exWoSh.Cells[5, intCol2 + 4].Font.Bold = true; //Выделить жирным
                    exWoSh.Columns[intCol2 + 4].ColumnWidth = 3.7;
                    exWoSh.Range[exWoSh.Cells[5, intCol2 + 4], exWoSh.Cells[6, intCol2 + 4]].Merge();


                    if (k == kodPredmet) //Предмет не изменился
                    {
                        exWoSh.Columns[intCol2 + 3].ColumnWidth = 3.7;
                        exWoSh.Range[exWoSh.Cells[4, intCol2 + 3], exWoSh.Cells[4, intCol2 + 4]]
                            .Merge();
                    }
                    else
                    {
                        k = kodPredmet;
                        exWoSh.Cells[4, intCol2 + 4].Value = rsPredm.Rows[i].ItemArray[1]; //Предмет
                        exWoSh.Cells[4, intCol2 + 4].Font.Bold = true; //Выделить жирным
                        exWoSh.Cells[4, intCol2 + 4].Orientation = 90;
                        exWoSh.Columns[intCol2 + 4].EntireColumn.AutoFit();
                        //exWoSh.Columns[intCol2 + 4].EntireColumn.AutoFit();
                    }
                    intCol2++;
                }

                if (m == 0 || m == 2) intCol2 += 4;



                for (var i = 0; i < rsStud.Rows.Count; i++)
                {

                    exWoSh.Cells[intRow + 5, 2].Value = rsStud.Rows[i].ItemArray[0]; //Код студента
                    exWoSh.Cells[intRow + 5, 3].Value = intRow - 1; //Порядковый номер
                    var str = rsStud.Rows[i].ItemArray[2].ToString();
                    exWoSh.Cells[intRow + 5, 4].Value = String.Format("{0} {1}.{2}.", rsStud.Rows[i].ItemArray[1],
                        str[0],
                        str[str.Length - 1]); //ФИО студента
                    exWoSh.Cells[intRow + 5, 4].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[intRow + 5, 5].Value = rsStud.Rows[i].ItemArray[3]; //Форма обучения студента

                    if (m == 0 || m == 2)
                    {
                        exWoSh.Cells[intRow + 5, intCol2].Value = rsProp.Rows[i].ItemArray[1];
                        //Пропусков по ув причине на предыдущей КН
                        exWoSh.Cells[intRow + 5, intCol2 + 3].Value = rsProp.Rows[i].ItemArray[2];
                        //Пропусков по неув причине на предыдущей КН

                        var foundRows = rsAVgBall.Select(String.Format("{1}='{0}'", rsStud.Rows[i].ItemArray[0],
                            rsAVgBall.Columns[0].ColumnName));

                        switch (foundRows.Length)
                        {
                            case 1:
                                //Средний балл студента на КН №6
                                exWoSh.Cells[intRow + 5, 1].Value = String.Format("{0:#.##}",
                                    foundRows[0].ItemArray[1]);
                                break;
                            case 0:
                                //На КН №6 баллы успеваемости студенту не проставлялись
                                exWoSh.Cells[intRow + 5, 1].Value = "2";
                                break;
                            default:
                                MessageBox.Show(@"Ошибка со студентом" + rsStud.Rows[i].ItemArray[1]);
                                break;
                        }
                    }

                    var pvz = rsPVz.Rows[0].ItemArray;
                    if (m == -1 || m == 1)
                    {
                        //Средний балл студента
                        exWoSh.Cells[intRow + 5, intCol2 + 6].FormulaR1C1 = String.Format(
                            "=IF(ISERROR(IF((RC{0}<>\"\"), ROUND(AVERAGE(RC{1}:RC7), 2), \"\")),\"\",IF((RC{0}<>\"\"), ROUND(AVERAGE(RC{1}:RC7), 2), \"\"))",
                            intCol2 + 5, intCol2 + 3);

                        //Лучшие/худшие
                        exWoSh.Cells[intRow + 5, 6].FormulaR1C1 = String.Format(
                            "=IF(RC{0}=\"\", IF(AND(RC{1}<{2}, RC{3}>={4}, RC{3}<>\"\"), \"Л\", \"\"), \"Н\")",
                            intCol2 + 7, intCol2 + 5, pvz[3], intCol2 + 6, pvz[6].ToString().Replace(',', '.'));

                        //Взыскание
                        exWoSh.Cells[intRow + 5, intCol2 + 7].FormulaR1C1 =
                            String.Format(
                                "=IF(RC[-2]<>\"\", IF(RC[-2]<{3}, IF({0}<RC[-1], \"\", IF(AND({1}<RC[-1], RC[-1]<={0}), \"Пред Усп\", IF(AND({2}<RC[-1], RC[-1]<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(AND({3}<=RC[-2], RC[-2]<{4}), IF({0}<RC[-1], \"Пред Проп\", IF(AND({1}<RC[-1]," +
                                " RC[-1]<={0}), \"Пред Проп и Усп\", IF(AND({2}<RC[-1], RC[-1]<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(AND({4}<=RC[-2], RC[-2]<{5}), IF({1}<RC[-1], \"Зам Проп\", IF(AND({2}<RC[-1], RC[-1]<={1}), \"Зам Проп и Усп\", \"Выг Усп\")), IF({2}<RC[-1], \"Выг Проп\", \"Выг Проп и Усп\")))),\"\")",
                                pvz[0].ToString().Replace(',', '.'), pvz[1].ToString().Replace(',', '.'),
                                pvz[2].ToString().Replace(',', '.'), pvz[3], pvz[4], pvz[5]);
                        exWoSh.Cells[intRow + 5, intCol2 + 7].Font.Bold = true; //Выделить жирным

                    }
                    else
                    {
                        //Средний балл студента
                        exWoSh.Cells[intRow + 5, intCol2 + 6].FormulaR1C1 = String.Format(
                            "=IF(ISERROR(IF((RC{0}<>\"\"), ROUND(AVERAGE(RC{1}:RC7), 2), \"\")),\"\",IF((RC{0}<>\"\"), ROUND(AVERAGE(RC{1}:RC7), 2), \"\"))",
                            intCol2 + 5, intCol2 - 1);
                        //Разность пропусков (№12-№6)
                        //По ув причине
                        exWoSh.Cells[intRow + 5, intCol2 + 1].FormulaR1C1 = String.Format(
                            "=IF((RC{0}<>\"\"), RC{0}-RC{1}, \"\")", intCol2 + 2, intCol2);
                        //По неув причине
                        exWoSh.Cells[intRow + 5, intCol2 + 4].FormulaR1C1 = String.Format(
                            "=IF((RC{0}<>\"\"), RC{0}-RC{1}, \"\")", intCol2 + 5, intCol2 + 3);
                        //Лучшие/худшие
                        exWoSh.Cells[intRow + 5, 6].FormulaR1C1 = String.Format(
                            "=IF(RC{0}=\"\", IF(AND(RC{1}<{2}, RC{3}>={4}, RC{3}<>\"\"), \"Л\", \"\"), \"Н\")",
                            intCol2 + 8, intCol2 + 5, pvz[7], intCol2 + 6, pvz[6].ToString().Replace(',', '.'));
                        //Взыскание №6
                        exWoSh.Cells[intRow + 5, intCol2 + 7].FormulaR1C1 = String.Format(
                            "=IF(RC1<>-1, IF(RC[-4]<{3}, IF({0}<RC1, \"\", IF(AND({1}<RC1, RC1<={0}), \"Пред Усп\", IF(AND({2}<RC1, RC1<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(AND({3}<=RC[-4], RC[-4]<{4}), " +
                            "IF({0}<RC1, \"Пред Проп\", IF(AND({1}<RC1, RC1<={0}), \"Пред Проп и Усп\", IF(AND({2}<RC1, RC1<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(AND({4}<=RC[-4], RC[-4]<{5}), " +
                            "IF({1}<RC1, \"Зам Проп\", IF(AND({2}<RC1, RC1<={1}), \"Зам Проп и Усп\", \"Выг Усп\")), IF({2}<RC1, \"Выг Проп\", \"Выг Проп и Усп\")))),\"\")",
                            pvz[0].ToString().Replace(',', '.'), pvz[1].ToString().Replace(',', '.'),
                            pvz[2].ToString().Replace(',', '.'), pvz[3], pvz[4], pvz[5]);
                        exWoSh.Cells[intRow + 5, intCol2 + 7].Style.Font.Bold = true;

                        //Взыскание №12
                        exWoSh.Cells[intRow + 5, intCol2 + 8].FormulaR1C1 = String.Format(
                            "=IF(RC[-3]<>\"\", IF(OR(RC[-4]<0, AND(RC[-5]<{3}, RC[-3]<{7}), AND({3}<=RC[-5], RC[-5]<{4}, {7}<=RC[-3], RC[-3]<{8}), AND({4}<=RC[-5], RC[-5]<{5}, {8}<=RC[-3], RC[-3]<{9}), AND({5}<=RC[-5], RC[-4]<{10})), " +
                            "IF({0}<RC[-2], \"\", IF(AND({1}<RC[-2], RC[-2]<={0}), \"Пред Усп\", IF(AND({2}<RC[-2], RC[-2]<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(AND(RC[-5]<{3}, {7}<=RC[-3], RC[-3]<{8}), IF({0}<RC[-2], \"Пред Проп\", " +
                            "IF(AND({1}<RC[-2], RC[-2]<={0}), \"Пред Проп и Усп\", IF(AND({2}<RC[-2], RC[-2]<={1}), \"Зам Усп\", \"Выг Усп\"))), IF(OR(AND(RC[-5]<{3}, {8}<=RC[-3], RC[-3]<{9}), AND({3}<=RC[-5], RC[-5]<{4}, {8}<=RC[-3], RC[-3]<{9})), " +
                            "IF({1}<RC[-2], \"Зам Проп\", IF(AND({2}<RC[-2], RC[-2]<={1}), \"Зам Проп и Усп\", \"Выг Усп\")), IF({2}<RC[-2], \"Выг Проп\", \"Выг Проп и Усп\")))),\"\")",
                            pvz[0].ToString().Replace(',', '.'), pvz[1].ToString().Replace(',', '.'),
                            pvz[2].ToString().Replace(',', '.'), pvz[3], pvz[4], pvz[5], pvz[6], pvz[7], pvz[8], pvz[9],
                            pvz[10]);
                        exWoSh.Cells[intRow + 5, intCol2 + 8].Style.Font.Bold = true;
                    }
                    intRow++;
                }


                //Скрыть спец поля
                exWoSh.Columns[1].EntireColumn.Hidden = true;
                exWoSh.Columns[2].EntireColumn.Hidden = true;
                exWoSh.Rows[1].EntireRow.Hidden = true;
                exWoSh.Rows[2].EntireRow.Hidden = true;
                exWoSh.Rows[3].EntireRow.Hidden = true;
                exWoSh.Columns[3].EntireColumn.AutoFit(); //Автовыравнивание
                exWoSh.Columns[4].EntireColumn.AutoFit(); //Автовыравнивание

                //Разрешить ввод баллов успеваемости и числа пропусков
                if (m == 0 || m == 2)
                {
                    exWoSh.Protection.AllowEditRanges.Add("Баллы",
                        exWoSh.Range[exWoSh.Cells[7, 7], exWoSh.Cells[intRow + 4, intCol2 - 1]]);
                    exWoSh.Protection.AllowEditRanges.Add("ПрУваж",
                        exWoSh.Range[
                            exWoSh.Cells[7, intCol2 + 2], exWoSh.Cells[intRow + 4, intCol2 + 2]]);
                    exWoSh.Protection.AllowEditRanges.Add("ПрНеув",
                        exWoSh.Range[
                            exWoSh.Cells[7, intCol2 + 5], exWoSh.Cells[intRow + 4, intCol2 + 5]]);
                }
                else
                    exWoSh.Protection.AllowEditRanges.Add("БаллПр",
                        exWoSh.Range[exWoSh.Cells[7, 7], exWoSh.Cells[intRow + 4, intCol2 + 5]]);

                //Сумма пропусков
                if (m == 0 || m == 2)
                {
                    exWoSh.Cells[intRow + 5, intCol2].FormulaR1C1 = String.Format(
                        "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2, intCol2);
                    exWoSh.Cells[intRow + 5, intCol2 + 1].FormulaR1C1 = String.Format(
                        "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2,
                        intCol2 + 1);
                    exWoSh.Cells[intRow + 5, intCol2 + 2].FormulaR1C1 = String.Format(
                        "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2,
                        intCol2 + 2);
                    exWoSh.Cells[intRow + 5, intCol2 + 3].FormulaR1C1 = String.Format(
                        "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2,
                        intCol2 + 3);
                }

                exWoSh.Cells[intRow + 5, intCol2 + 4].FormulaR1C1 = String.Format(
                    "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2, intCol2 + 4);
                exWoSh.Cells[intRow + 5, intCol2 + 5].FormulaR1C1 = String.Format(
                    "=IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,SUM(R[-{0}]C{1}:R[-1]C{1}),\"\")", intRow - 2, intCol2 + 5);

                //Средний балл группы
                exWoSh.Cells[intRow + 5, intCol2 + 6].FormulaR1C1 = String.Format(
                    "=IF(ISERROR(IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,AVERAGE(R[-{0}]C{1}:R[-1]C{1}),\"\")),\"\"," +
                    "IF(SUM(R[-{0}]C{1}:R[-1]C{1})<>0,AVERAGE(R[-{0}]C{1}:R[-1]C{1}),\"\"))", intRow - 2,
                    intCol2 + 6);


                //Скрыть формулы
                exWoSh.Range[exWoSh.Cells[7, 6], exWoSh.Cells[intRow + 4, 6]].FormulaHidden = true; //Столбец Л/Н
                if (m == 0 || m == 2)
                {
                    exWoSh.Range[exWoSh.Cells[7, intCol2 + 1], exWoSh.Cells[intRow + 4, intCol2 + 1]]
                        .FormulaHidden = true; //Столбец разницы пропусков
                    exWoSh.Range[exWoSh.Cells[7, intCol2 + 4], exWoSh.Cells[intRow + 4, intCol2 + 4]]
                        .FormulaHidden = true; //Столбец разницы пропусков
                    exWoSh.Range[exWoSh.Cells[7, intCol2 + 6], exWoSh.Cells[intRow + 4, intCol2 + 8]]
                        .FormulaHidden = true; //Столбцы среднего балла и взыскания
                    exWoSh.Range[
                        exWoSh.Cells[intRow + 5, intCol2], exWoSh.Cells[intRow + 5, intCol2 + 6]]
                        .FormulaHidden = true; //Строка сум
                }
                else
                {
                    exWoSh.Range[exWoSh.Cells[7, intCol2 + 6], exWoSh.Cells[intRow + 4, intCol2 + 7]]
                        .FormulaHidden = true; //Столбцы среднего балла и взыскания
                    exWoSh.Range[exWoSh.Cells[intRow + 5, intCol2 + 4], exWoSh.Cells[intRow + 5, intCol2 + 6]]
                        .FormulaHidden = true; //Строка сумм
                }

                //Сетка
                if (m == 0 || m == 2) intCol2 = intCol2 + 1;

                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeLeft].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeTop].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeRight].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlEdgeRight].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlInsideVertical].Weight = Excel.XlBorderWeight.xlThin;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                exWoSh.Range[exWoSh.Cells[4, 3], exWoSh.Cells[intRow + 10, intCol2 + 6]].Borders[
                    Excel.XlBordersIndex.xlInsideHorizontal].Weight = Excel.XlBorderWeight.xlThin;
                if (m == 0 || m == 2) intCol2 = intCol2 - 1;

                //Поля для подписи
                exWoSh.Range[exWoSh.Cells[intRow + 6, 5], exWoSh.Cells[intRow + 6, intCol2 + 6]].Merge();
                exWoSh.Cells[intRow + 6, 5].Value = "Подпись, расшифровка подписи:";
                exWoSh.Cells[intRow + 11, 4].Value =
                    "Примечание: допустимые значения в ячейках баллов - \"\", \"0\", \"1\" или \"2\", в ячейках пропусков - 0 или больше";
                exWoSh.Cells[7, 7].Select(); //Установка выделения в верхний левый угол


                //Поля пропусков
                if (m == -1 || m == 1)
                {
                    exWoSh.Cells[4, intCol2 + 4].Value = "Пропуски";
                    exWoSh.Cells[4, intCol2 + 4].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    //Центровака по вертикали
                    exWoSh.Cells[4, intCol2 + 4].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[5, intCol2 + 4].Value = "Ув. пр.";
                    exWoSh.Cells[5, intCol2 + 4].WrapText = true;
                    exWoSh.Cells[5, intCol2 + 4].VerticalAlignment = Excel.XlVAlign.xlVAlignJustify;
                    //Центровака по вертикали
                    exWoSh.Cells[5, intCol2 + 4].Font.Bold = true; //Выделить жирным
                    exWoSh.Range[exWoSh.Cells[4, intCol2 + 4], exWoSh.Cells[4, intCol2 + 5]].Merge();
                    exWoSh.Range[exWoSh.Cells[5, intCol2 + 4], exWoSh.Cells[6, intCol2 + 4]].Merge();
                    exWoSh.Columns[intCol2 + 4].ColumnWidth = 5;

                    exWoSh.Cells[5, intCol2 + 5].Value = "Неув. пр.";
                    exWoSh.Cells[5, intCol2 + 5].WrapText = true;
                    exWoSh.Cells[5, intCol2 + 5].VerticalAlignment = Excel.XlVAlign.xlVAlignJustify;
                    //Центровака по вертикали
                    exWoSh.Cells[5, intCol2 + 5].Font.Bold = true; //Выделить жирным
                    exWoSh.Range[exWoSh.Cells[5, intCol2 + 5], exWoSh.Cells[6, intCol2 + 5]].Merge();
                    exWoSh.Columns[intCol2 + 5].ColumnWidth = 5;
                }
                else
                {
                    exWoSh.Cells[4, intCol2].Value = "Пропуски";
                    exWoSh.Cells[4, intCol2].Font.Size = 18; //Установка размера шрифта
                    exWoSh.Cells[4, intCol2].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    //Центровака по вертикали
                    exWoSh.Cells[4, intCol2].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[5, intCol2].Value = "Ув. пр.";
                    exWoSh.Cells[5, intCol2].WrapText = true;
                    exWoSh.Cells[5, intCol2].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[5, intCol2].VerticalAlignment = Excel.XlVAlign.xlVAlignJustify;
                    //Центровака по вертикали
                    exWoSh.Range[exWoSh.Cells[4, intCol2], exWoSh.Cells[4, intCol2 + 5]].Merge();
                    exWoSh.Range[exWoSh.Cells[5, intCol2], exWoSh.Cells[5, intCol2 + 2]].Merge();
                    exWoSh.Cells[6, intCol2].Value = "№6";
                    exWoSh.Cells[6, intCol2].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[6, intCol2 + 1].Value = "-";
                    exWoSh.Cells[6, intCol2 + 1].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[6, intCol2 + 2].Value = "№ 12";
                    exWoSh.Cells[6, intCol2 + 2].Font.Bold = true; //Выделить жирным
                    exWoSh.Columns[intCol2].ColumnWidth = 4.5;
                    exWoSh.Columns[intCol2 + 1].ColumnWidth = 4.5;
                    exWoSh.Columns[intCol2 + 2].ColumnWidth = 4.5;

                    exWoSh.Cells[5, intCol2 + 3].Value = "Неув. пр.";
                    exWoSh.Cells[5, intCol2 + 3].WrapText = true;
                    exWoSh.Cells[5, intCol2 + 3].VerticalAlignment = Excel.XlVAlign.xlVAlignJustify;
                    //Центровака по вертикали
                    exWoSh.Cells[5, intCol2 + 3].Font.Bold = true; //Выделить жирным
                    exWoSh.Range[exWoSh.Cells[5, intCol2 + 3], exWoSh.Cells[5, intCol2 + 5]].Merge();
                    exWoSh.Cells[6, intCol2 + 3].Value = "№6";
                    exWoSh.Cells[6, intCol2 + 3].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[6, intCol2 + 4].Value = "-";
                    exWoSh.Cells[6, intCol2 + 4].Font.Bold = true; //Выделить жирным
                    exWoSh.Cells[6, intCol2 + 5].Value = "№ 12";
                    exWoSh.Cells[6, intCol2 + 5].Font.Bold = true; //Выделить жирным
                    exWoSh.Columns[intCol2 + 3].ColumnWidth = 4.5;
                    exWoSh.Columns[intCol2 + 4].ColumnWidth = 4.5;
                    exWoSh.Columns[intCol2 + 5].ColumnWidth = 4.5;
                    exWoSh.Cells[4, intCol2 + 7].Value = "Предыдущее взыскание";
                    exWoSh.Cells[4, intCol2 + 7].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    //Центровака по вертикали
                    exWoSh.Cells[4, intCol2 + 7].Font.Bold = true; //Выделить жирным
                    exWoSh.Range[exWoSh.Cells[4, intCol2 + 7], exWoSh.Cells[6, intCol2 + 7]].Merge();
                    exWoSh.Columns[intCol2 + 7].ColumnWidth = 14.8;
                }

                //Поле Средний балл
                exWoSh.Range[exWoSh.Cells[4, intCol2 + 6], exWoSh.Cells[6, intCol2 + 6]].Merge();
                exWoSh.Cells[4, intCol2 + 6].Value = "Средний балл";
                exWoSh.Cells[4, intCol2 + 6].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                exWoSh.Cells[4, intCol2 + 6].Orientation = 90;
                exWoSh.Cells[4, intCol2 + 6].Font.Bold = true; //Выделить жирным
                exWoSh.Columns[intCol2 + 6].ColumnWidth = 4;
                if (m == -1 || m == 1)
                {
                    exWoSh.Columns[intCol2 + 7].ColumnWidth = 0.15;
                    exWoSh.Columns[intCol2 + 8].ColumnWidth = 11;
                    exWoSh.Columns[intCol2 + 9].ColumnWidth = 2.83;
                }
                else
                {
                    exWoSh.Columns[intCol2 + 8].ColumnWidth = 0.15;
                    exWoSh.Columns[intCol2 + 9].ColumnWidth = 11;
                    exWoSh.Columns[intCol2 + 10].ColumnWidth = 2.83;
                }


                //Параметры листа
                exWoSh.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape; //Альбомный лист
                exWoSh.PageSetup.Zoom = false; //Вместить лист
                exWoSh.PageSetup.FitToPagesWide = 1;
                exWoSh.PageSetup.FitToPagesTall = 1; //на одну страницу
                exWoSh.PageSetup.LeftMargin = 0; //Параметры
                exWoSh.PageSetup.RightMargin = 0; //листа
                exWoSh.PageSetup.TopMargin = 0;
                exWoSh.PageSetup.BottomMargin = 0;
                exWoSh.PageSetup.HeaderMargin = 0; //для
                exWoSh.PageSetup.FooterMargin = 0; //печати

                //Защитить лист паролем
                exWoSh.Protect("221183", false, true, true);

                exApp.ScreenUpdating = true;
                // Запрашивать сохранение
                exApp.DisplayAlerts = false;
                //Создаём файл
                exWorkbook.SaveAs(
                    Directory.GetCurrentDirectory() + @"\Бланки успеваемости\" + otd + @"\" + nameGrup +
                    @".xls", Excel.XlFileFormat.xlExcel8);

                exWorkbook.Close(false, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                exWorkbooks.Close();

                exWorkbooks = null;
                exWorkbook = null;
                exWoSh = null;
                exSheets = null;
            }
            catch (Exception z)
            {
                MessageBox.Show(@"Покрашились   " + z.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(@"Вы уверены, что хотите подготовить бланки успеваемости?", "", MessageBoxButtons.YesNo) ==
                DialogResult.No) return;

            var dirCur = Directory.GetCurrentDirectory();
            if (cB_Group.Checked)
            {
                // Резервное копирование приказов и бланков за предыдущую контрольную неделю
                if (Directory.Exists(Directory.GetCurrentDirectory() + @"\Бланки успеваемости\"))
                {
                    if (!Directory.Exists(dirCur + @"\Архив\"))
                        Directory.CreateDirectory(dirCur + @"\Архив\");

                    string papka = null;
                    if (month > 0 && month < 4) papka = @"\Архив\" + year + @"_осень_2\";
                    else if (month > 3 && month < 9) papka = @"\Архив\" + year + @"_весна_1\";
                    else if (month > 8 && month < 11) papka = @"\Архив\" + year + @"_весна_2\";
                    else if (month > 10 && month < 13) papka = @"\Архив\" + year + @"_осень_1\";

                    if (!Directory.Exists(dirCur + papka))
                        Directory.CreateDirectory(dirCur + papka);
                    // копируем приказы
                    if (Directory.Exists(dirCur + @"Приказы"))
                    {
                        if (Directory.Exists(dirCur + papka + @"Приказы"))
                            Directory.Delete(dirCur + papka + @"Приказы", true);
                        CopyDir(dirCur + @"Приказы", dirCur + papka + @"Приказы");
                    }
                    // копируем бланки успеваемости
                    if (Directory.Exists(dirCur + @"\Бланки успеваемости"))
                    {
                        if (Directory.Exists(dirCur + papka + @"\Бланки успеваемости"))
                            Directory.Delete(dirCur + papka + @"\Бланки успеваемости", true);
                        CopyDir(dirCur + @"\Бланки успеваемости", dirCur + papka + @"\Бланки успеваемости");
                    }
                    // копируем бд
                    File.Copy(dirCur + @"\" + nameDb, dirCur + papka + nameDb, true);
                    // удаляем старые бланки
                    Directory.Delete(dirCur + @"\Бланки успеваемости\", true);
                }
                // создаем заново папки
                Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФКТЭ\");
                Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭУ\");
                Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭЭ\");
            }
            else
            {
                if (!Directory.Exists(dirCur + @"\Бланки успеваемости\"))
                {
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФКТЭ\");
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭУ\");
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭЭ\");
                }
                if (!Directory.Exists(dirCur + @"\Бланки успеваемости\ФКТЭ\"))
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФКТЭ\");
                if (!Directory.Exists(dirCur + @"\Бланки успеваемости\ФЭУ\"))
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭУ\");
                if (!Directory.Exists(dirCur + @"\Бланки успеваемости\ФЭЭ\"))
                    Directory.CreateDirectory(dirCur + @"\Бланки успеваемости\ФЭЭ\");
            }

            var m = 0;
            if (month > 0 && month < 4) m = -1;
            else if (month > 3 && month < 9) m = 0;
            else if (month > 8 && month < 11) m = 1;
            else if (month > 10 && month < 13) m = 2;

            //Параметры для взысканий
            var rsPVzDt = new OleDbDataAdapter(new OleDbCommand("SELECT * FROM Парам_Взыск", _connection));
            var rsPVz = new DataTable();
            rsPVzDt.Fill(rsPVz);

            //Данные для экспорта
            var t1 = year - 2000;
            var t2 = (month / 9);

            var strSql = String.Format("SELECT * FROM Группы" +
                " WHERE ((2 *({0} - Right(([Группы].[Группа]), 2)) + {1}) <= 8) ORDER BY Группы.КодГр",
                    t1, t2);
            var rsGrupDa = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var rsGrup = new DataTable();
            rsGrupDa.Fill(rsGrup);


            pBar.Maximum = rsGrup.Rows.Count + 1;
            pBar.Value = 1;

            List<string> selectGrup = null;
            if (!cB_Group.Checked)
            {
                selectGrup = new List<string>();
                for (var i = 0; i < listBox1.Items.Count; i++)
                {
                    if (!listBox1.GetSelected(i)) continue;
                    var t = (DataRowView)(listBox1.Items[i]);
                    selectGrup.Add(t.Row.ItemArray[0].ToString());
                }
            }


            //***********************************************************************//
            //********************         Магия         ****************************//
            var swatch = new Stopwatch();
            swatch.Start(); // начинаем считать время на костование
            exApp = new Excel.Application
            {
                Visible = false,
                DefaultSaveFormat = Excel.XlFileFormat.xlExcel8,
                SheetsInNewWorkbook = 1
            };
            for (var ii = 0; ii < rsGrup.Rows.Count; ii++)
            {
                if (!cB_Group.Checked)
                {
                    var sel = selectGrup.IndexOf(rsGrup.Rows[ii].ItemArray[1].ToString());
                    if (sel >= 0) genXl(rsGrup.Rows[ii], rsPVz, m, t1);
                }
                else
                    genXl(rsGrup.Rows[ii], rsPVz, m, t1);
                pBar.Value++;
            }
            // уничтожение следов магии
            exApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(exApp);
            exApp = null;
            GC.Collect();
            //***********************************************************************//

            swatch.Stop(); // стоп   
            this.Text = swatch.Elapsed.ToString(); // результат  

            MessageBox.Show(@"Выполнено успешно.");
        }

        private static void CopyDir(string fromDir, string toDir)
        {
            Directory.CreateDirectory(toDir);
            foreach (string s1 in Directory.GetFiles(fromDir))
            {
                string s2 = toDir + "\\" + Path.GetFileName(s1);
                File.Copy(s1, s2);
            }
            foreach (string s in Directory.GetDirectories(fromDir))
            {
                CopyDir(s, toDir + "\\" + Path.GetFileName(s));
            }
        }

        private void DeleteBd(string sql)
        {
            var dataAdapter = new OleDbDataAdapter(new OleDbCommand(sql, _connection));
            var dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            for (var i = 0; i < dataTable.Rows.Count; i++) dataTable.Rows[i].Delete();
            var commandBuilder = new OleDbCommandBuilder(dataAdapter) { ConflictOption = ConflictOption.OverwriteChanges };
            dataAdapter.Update(dataTable);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(@"Вы уверены, что хотите загрузить данные ?", "", MessageBoxButtons.YesNo) == DialogResult.No) return;

            //Проверка наличия директории
            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Бланки успеваемости\"))
            {
                MessageBox.Show(@"Папка со сведениями отсутствует !", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var t1 = year - 2000;
            var t2 = (month / 9);

            //Параметры для взысканий
            var strSql = String.Format("SELECT Группы.Группа, Группы.Уч_Отдел FROM Группы INNER JOIN Предм_Гр_Семестр ON Группы.КодГр = Предм_Гр_Семестр.Группа" +
                   " GROUP BY Группы.Группа, Группы.Уч_Отдел, Предм_Гр_Семестр.Семестр, Группы.КодГр HAVING (((Предм_Гр_Семестр.Семестр) = 2 * ({0} - Right(([Группы].[Группа]), 2)) + {1})) ORDER BY Группы.КодГр",
                       t1, t2);
            var impGrDt = new OleDbDataAdapter(new OleDbCommand(strSql, _connection));
            var impGr = new DataTable();
            impGrDt.Fill(impGr);


            if (month > 0 && month < 4) t2 = -1;
            else if (month > 3 && month < 9) t2 = 0;
            else if (month > 8 && month < 11) t2 = 1;
            else if (month > 10 && month < 13) t2 = 2;

            List<string> selectGrup = null;
            if (cB_Group.Checked)
            {
                //удаление баллов в текущей КН, если загрузка выполняется повторно
                strSql = String.Format("SELECT Балл_КН.* " +
                        "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт" +
                        " WHERE ((Балл_КН.№КН = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1}))", t1, t2);
                DeleteBd(strSql);

                //удаление числа пропусков в текущей КН, если загрузка выполняется повторно
                strSql = String.Format("SELECT Успеваемость.* " +
                        "FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент" +
                        " WHERE ((Успеваемость.№КН = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1}))", t1, t2);
                DeleteBd(strSql);
            }
            else
            {
                selectGrup = new List<string>();
                for (var j = 0; j < listBox1.Items.Count; j++)
                {
                    if (!listBox1.GetSelected(j)) continue;
                    var t = (DataRowView)(listBox1.Items[j]);
                    selectGrup.Add(t.Row.ItemArray[0].ToString());

                    //удаление баллов в текущей КН, если загрузка выполняется повторно
                    strSql = String.Format("SELECT Балл_КН.*" +
                        " FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Балл_КН ON Студенты.КодСт = Балл_КН.КодСт" +
                        " WHERE ((Балл_КН.№КН = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1}) AND (Группы.Группа)=\"{2}\")", t1, t2, t.Row.ItemArray[1].ToString());
                    DeleteBd(strSql);

                    //удаление числа пропусков в текущей КН, если загрузка выполняется повторно
                    strSql = String.Format("SELECT Успеваемость.*" +
                        " FROM (Группы INNER JOIN Студенты ON Группы.КодГр = Студенты.Группа) INNER JOIN Успеваемость ON Студенты.КодСт = Успеваемость.Студент" +
                        " WHERE ((Успеваемость.№КН = 4 * ({0} - Right(([Группы].[Группа]), 2)) + {1}) AND (Группы.Группа)=\"{2}\")", t1, t2, t.Row.ItemArray[1].ToString());
                    DeleteBd(strSql);
                }
            }

            var rsBallDa = new OleDbDataAdapter(new OleDbCommand(@"SELECT * FROM Балл_КН", _connection));
            var rsBallKn = new DataTable();
            rsBallDa.Fill(rsBallKn);

            var rsPropDa = new OleDbDataAdapter(new OleDbCommand(@"SELECT * FROM Успеваемость", _connection));
            var rsProp = new DataTable();
            rsPropDa.Fill(rsProp);


            pBar.Maximum = listBox1.Items.Count;
            pBar.Value = 0;

            int i;
            for (i = 0; i < impGr.Rows.Count; i++)
            {
                if (!cB_Group.Checked)
                {
                    var sel = selectGrup.IndexOf(impGr.Rows[i].ItemArray[0].ToString());
                    if (sel < 0)
                    {
                        pBar.Value = i;
                        continue;
                    }
                }

                string otd = null;

                switch (Convert.ToInt32(impGr.Rows[i].ItemArray[1]))
                {
                    case 1:
                        otd = "ФКТЭ";
                        break;
                    case 2:
                        otd = "ФЭУ";
                        break;
                    case 3:
                        otd = "ФЭЭ";
                        break;
                    default:
                        MessageBox.Show(@"Проблема с факультетом");
                        break;
                }

                var path = Directory.GetCurrentDirectory() + @"\Бланки успеваемости\" + otd + @"\" +
                           Convert.ToString(impGr.Rows[i].ItemArray[0]) + ".xls";

                if (!File.Exists(path))
                {
                    if (MessageBox.Show("Найти его?\n Да - откроеться окно выбора файлов\n Нет - выйти из загрузки документов",
                        @"Внимание отсутствует документ", MessageBoxButtons.YesNo, MessageBoxIcon.Error) ==
                        DialogResult.Yes)
                    {
                        var ofd = new OpenFileDialog
                        {
                            DefaultExt = "*.xls",
                            Filter = @"Excel (*.xls)|*.xls"
                        };

                        if (ofd.ShowDialog() != DialogResult.OK) return;
                        path = ofd.FileName;
                    }
                    else
                        return;
                }

                var workbook = ExcelWorkbook.ReadXLS(path);
                var actSheet = workbook.Worksheets[0];

                var v = 1;
                var stolb = 6;
                while (Convert.ToString(actSheet.Cells[v, stolb].Value) != "") stolb = stolb + 1;
                stolb = stolb - 1;

                string errStr = null;
                if (stolb == 5)
                {
                    errStr = Convert.ToString(impGr.Rows[i].ItemArray[0]) +
                             " - не введены предметы или системные поля ведомости повреждены !  ";
                    if (MessageBox.Show(errStr, @"Завершить загрузку?", MessageBoxButtons.YesNo) == DialogResult.Yes) return;
                    continue;
                }
                else
                {
                    const int offset = 6;
                    var a = new int[stolb - offset + 1];
                    for (var j = 0; j < stolb - offset + 1; j++) a[j] = Convert.ToInt32(actSheet.Cells[v, j + offset].Value);
                    v = v + 2;

                    if (Convert.ToString(actSheet.Cells[v, 1].Value) == "")
                    {
                        errStr = Convert.ToString(impGr.Rows[i].ItemArray[0]) +
                             " - повреждено системное поле номера контрольной недели !  ";
                        if (MessageBox.Show(errStr, @"Завершить загрузку?", MessageBoxButtons.YesNo) == DialogResult.Yes) return;
                        continue;
                    }
                    else
                    {
                        var kn = Convert.ToInt32(actSheet.Cells[v, 1].Value);
                        v = v + 1;
                        if (Convert.ToString(actSheet.Cells[v, 1].Value) == "")
                        {
                            errStr = Convert.ToString(impGr.Rows[i].ItemArray[0]) +
                                " - повреждено системное поле группы !  ";
                            if (MessageBox.Show(errStr, @"Завершить загрузку?", MessageBoxButtons.YesNo) == DialogResult.Yes) return;
                            continue;
                        }
                        else
                        {
                            //Gruppa = Convert.ToInt32(actSheet.Cells[v, 1].Value);
                            v = v + 2;
                            if (Convert.ToString(actSheet.Cells[v, 1].Value) == "")
                            {
                                errStr = Convert.ToString(impGr.Rows[i].ItemArray[0]) +
                                    " - не введены фамилии студентов или повреждено системное поле кодов студентов !  ";
                                if (MessageBox.Show(errStr, @"Завершить загрузку?", MessageBoxButtons.YesNo) == DialogResult.Yes) return;
                                continue;
                            }
                            else
                            {
                                while (Convert.ToString(actSheet.Cells[v, 1].Value) != "")
                                {
                                    for (var j = 0; j < stolb - offset + 1; j++)
                                    {
                                        var ball = Convert.ToInt32(actSheet.Cells[v, j + offset].Value);
                                        if (ball >= 0 && ball < 3)
                                        {
                                            var row = rsBallKn.NewRow();
                                            row[0] = a[j];
                                            row[1] = actSheet.Cells[v, 1].Value;
                                            row[2] = kn;
                                            row[3] = ball;
                                            rsBallKn.Rows.Add(row);

                                            // var rsBallCb = new OleDbCommandBuilder(rsBallDa) { ConflictOption = ConflictOption.OverwriteChanges };
                                            // rsBallDa.Update(rsBallKn); // update
                                        }
                                    }
                                    var rsBallCb = new OleDbCommandBuilder(rsBallDa) { ConflictOption = ConflictOption.OverwriteChanges };
                                    rsBallDa.Update(rsBallKn); // update

                                    if ((t2 == -1) || (t2 == 1))
                                    {
                                        var row = rsProp.NewRow();

                                        row[1] = actSheet.Cells[v, 1].Value;
                                        row[2] = kn;
                                        if (Convert.ToString(actSheet.Cells[v, stolb + 1].Value) != "")
                                        {
                                            row[3] = actSheet.Cells[v, stolb + 1].Value;
                                        }

                                        if (Convert.ToString(actSheet.Cells[v, stolb + 2].Value) != "")
                                        {
                                            row[4] = actSheet.Cells[v, stolb + 2].Value;
                                        }

                                        rsProp.Rows.Add(row);
                                    }
                                    else
                                    {
                                        var row = rsProp.NewRow();

                                        row[1] = actSheet.Cells[v, 1].Value;
                                        row[2] = kn;
                                        if (Convert.ToString(actSheet.Cells[v, stolb + 3].Value) != "")
                                        {
                                            row[3] = actSheet.Cells[v, stolb + 3].Value;
                                        }

                                        if (Convert.ToString(actSheet.Cells[v, stolb + 6].Value) != "")
                                        {
                                            row[4] = actSheet.Cells[v, stolb + 6].Value;
                                        }

                                        rsProp.Rows.Add(row);
                                    }

                                    var rsPropCb = new OleDbCommandBuilder(rsPropDa) { ConflictOption = ConflictOption.OverwriteChanges };
                                    rsPropDa.Update(rsProp); // update
                                    v++;
                                }
                            }

                        }
                    }
                }

                pBar.Value = i;
            }

            MessageBox.Show(@"Загрузка успеваемости завершена !");
        }

        private void cB_Group_CheckedChanged(object sender, EventArgs e)
        {
            listBox1.Enabled = !cB_Group.Checked;
        }

        private void LoadData_FormClosing(object sender, FormClosingEventArgs e)
        {
            Statics.MainFrm.Show();
            Statics.MainFrm.Activate();
        }
    }
}
