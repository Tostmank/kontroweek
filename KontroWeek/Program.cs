﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace KontroWeek
{
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Statics.MainFrm = new Form1();
            Application.Run(Statics.MainFrm); 
        }
    }
}
